﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="11008008">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="varPersistentID:{02445FFA-5A5B-4FD8-B24A-28909BF2F050}" Type="Ref">/My Computer/Criterias.lvlib/ForceCriteria:1</Property>
	<Property Name="varPersistentID:{11A452BC-2108-4A61-8C1D-03558DAB93AB}" Type="Ref">/My Computer/Criterias.lvlib/PressureCriteria:1</Property>
	<Property Name="varPersistentID:{3AC0F1B7-A88C-4A60-B344-FF8FB307CBA9}" Type="Ref">/My Computer/TestData.lvlib/Force:1</Property>
	<Property Name="varPersistentID:{4CFF32BE-D0D3-4640-A893-330F73627C03}" Type="Ref">/My Computer/SequenceData.lvlib/CurrentStep:1</Property>
	<Property Name="varPersistentID:{5C4CB5E5-D895-4B97-957C-E410EDBF7FF1}" Type="Ref">/My Computer/SequenceLogic.lvlib/Pause:1</Property>
	<Property Name="varPersistentID:{6E6541BF-1E40-4200-B25B-8BD25B8B443F}" Type="Ref">/My Computer/SequenceLogic.lvlib/Criteria:1</Property>
	<Property Name="varPersistentID:{7868DC75-F334-45B3-84BC-B1C1833B6B85}" Type="Ref">/My Computer/SequenceData.lvlib/StateTime:1</Property>
	<Property Name="varPersistentID:{7D969CB2-C206-45CC-A504-894F1A681244}" Type="Ref">/My Computer/Criterias.lvlib/SpeedCriteria:1</Property>
	<Property Name="varPersistentID:{8861A5A4-696A-40A1-827D-1912219C67C1}" Type="Ref">/My Computer/TestData.lvlib/Pressure:1</Property>
	<Property Name="varPersistentID:{A69E5083-7745-4360-9909-04817C7C6BBC}" Type="Ref">/My Computer/TestData.lvlib/Force:2</Property>
	<Property Name="varPersistentID:{DC24817C-FB32-4627-B513-FEEFE2BBBAF1}" Type="Ref">/My Computer/TestData.lvlib/Speed:1</Property>
	<Property Name="varPersistentID:{E7286000-65FD-4AF6-B28E-A1A2063B4A8F}" Type="Ref">/My Computer/SequenceLogic.lvlib/NextState:1</Property>
	<Property Name="varPersistentID:{F539C77F-8B1E-4FE3-9048-24EC08020D8F}" Type="Ref">/My Computer/SequenceData.lvlib/CurrentState:1</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="files" Type="Folder" URL="../files">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="TestData.lvlib" Type="Library" URL="../TestData.lvlib"/>
		<Item Name="Criterias.lvlib" Type="Library" URL="../Criterias.lvlib"/>
		<Item Name="SequenceData.lvlib" Type="Library" URL="../SequenceData.lvlib"/>
		<Item Name="SequenceLogic.lvlib" Type="Library" URL="../SequenceLogic.lvlib"/>
		<Item Name="SequenceControllerExample.vi" Type="VI" URL="../SequenceControllerExample.vi"/>
		<Item Name="SequenceGuiExample.vi" Type="VI" URL="../SequenceGuiExample.vi"/>
		<Item Name="SequenceSimpleExample.vi" Type="VI" URL="../SequenceSimpleExample.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="Check for Equality.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for Equality.vi"/>
				<Item Name="DWDT Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Error Code.vi"/>
				<Item Name="WDT Append Waveforms DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms DBL.vi"/>
				<Item Name="WDT Append Waveforms I64.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I64.vi"/>
				<Item Name="WDT Append Waveforms I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I32.vi"/>
				<Item Name="WDT Append Waveforms I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I16.vi"/>
				<Item Name="WDT Append Waveforms EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms EXT.vi"/>
				<Item Name="WDT Append Waveforms CXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms CXT.vi"/>
				<Item Name="WDT Append Waveforms CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms CDB.vi"/>
				<Item Name="Append Waveforms.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Append Waveforms.vi"/>
				<Item Name="NI_Excel.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Excel/NI_Excel.lvclass"/>
				<Item Name="NI_ReportGenerationCore.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_ReportGenerationCore.lvlib"/>
				<Item Name="NI_report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_report.lvclass"/>
				<Item Name="Built App File Layout.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/Built App File Layout.vi"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="NI_Standard Report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Standard Report/NI_Standard Report.lvclass"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Write BMP Data To Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data To Buffer.vi"/>
				<Item Name="Write BMP Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Write BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP File.vi"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="Write JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Write JPEG File.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="NI_HTML.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/HTML/NI_HTML.lvclass"/>
				<Item Name="Write PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Write PNG File.vi"/>
				<Item Name="Create Error Clust.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create Error Clust.vi"/>
				<Item Name="Destroy ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Destroy ActiveX Event Queue.vi"/>
				<Item Name="OccFireType.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/OccFireType.ctl"/>
				<Item Name="EventData.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/EventData.ctl"/>
				<Item Name="Wait On ActiveX Event.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait On ActiveX Event.vi"/>
				<Item Name="Wait types.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait types.ctl"/>
				<Item Name="Create ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create ActiveX Event Queue.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Generate Temporary File Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Generate Temporary File Path.vi"/>
				<Item Name="NI_ReportGenerationToolkit.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_office/NI_ReportGenerationToolkit.lvlib"/>
				<Item Name="Read JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Read JPEG File.vi"/>
				<Item Name="Handle Open Word or Excel File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Toolkit/Handle Open Word or Excel File.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="ni_tagger_lv_FlushAllConnections.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_FlushAllConnections.vi"/>
				<Item Name="NI_VariableUtilities.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Variable/NI_VariableUtilities.lvlib"/>
				<Item Name="NI_PtbyPt.lvlib" Type="Library" URL="/&lt;vilib&gt;/ptbypt/NI_PtbyPt.lvlib"/>
				<Item Name="XControlSupport.lvlib" Type="Library" URL="/&lt;vilib&gt;/_xctls/XControlSupport.lvlib"/>
				<Item Name="Version To Dotted String.vi" Type="VI" URL="/&lt;vilib&gt;/_xctls/Version To Dotted String.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DTbl Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital to Boolean Array.vi"/>
				<Item Name="DWDT Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital to Boolean Array.vi"/>
				<Item Name="Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Digital to Boolean Array.vi"/>
				<Item Name="DTbl Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Compress Digital.vi"/>
				<Item Name="DTbl Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Analog to Digital.vi"/>
				<Item Name="DWDT Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Analog to Digital.vi"/>
				<Item Name="Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Analog to Digital.vi"/>
				<Item Name="NI_MABase.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MABase.lvlib"/>
			</Item>
			<Item Name="GetConcreteClasses.vi" Type="VI" URL="../../../main/GUI/SubPanelUtilities/GetConcreteClasses.vi"/>
			<Item Name="RegulatorLister.xctl" Type="XControl" URL="../../../main/ControlSystem/Regulator/XControl/RegulatorLister/RegulatorLister.xctl"/>
			<Item Name="RegulatorConfiguration.xctl" Type="XControl" URL="../../../main/ControlSystem/Regulator/XControl/RegulatorConfiguration/RegulatorConfiguration.xctl"/>
			<Item Name="SequenceLister.xctl" Type="XControl" URL="../../../main/Sequence/SequenceHandler/SequenceLister/SequenceLister.xctl"/>
			<Item Name="Sequence.xctl" Type="XControl" URL="../../../main/Sequence/SequenceInputRunner/XControl/Sequence.xctl"/>
			<Item Name="Regulator.lvclass" Type="LVClass" URL="../../../main/ControlSystem/Regulator/Regulator.lvclass"/>
			<Item Name="FloatingPointSequence.lvclass" Type="LVClass" URL="../../../main/Sequence/FloatingPointSequence/FloatingPointSequence.lvclass"/>
			<Item Name="ChannelIdDelimiter.ctl" Type="VI" URL="../../../main/Sequence/SequenceInputRunner/ChannelIdDelimiter.ctl"/>
			<Item Name="SequenceSeriesType.ctl" Type="VI" URL="../../../main/Sequence/SequenceInputRunner/SequenceSeriesType.ctl"/>
			<Item Name="SimulatedBufferedDeviceReader.lvclass" Type="LVClass" URL="../../../drivers/DAQ/SimulatedBufferedDeviceReader/SimulatedBufferedDeviceReader.lvclass"/>
			<Item Name="SimulatedUnbufferedDeviceWriter.lvclass" Type="LVClass" URL="../../../drivers/DAQ/SimulatedUnbufferedDeviceWriter/SimulatedUnbufferedDeviceWriter.lvclass"/>
			<Item Name="FeedbackController.lvclass" Type="LVClass" URL="../../../main/ControlSystem/FeedbackController/FeedbackController.lvclass"/>
			<Item Name="PIDRegulator.lvclass" Type="LVClass" URL="../../../main/ControlSystem/Concrete/PIDRegulator/PIDRegulator.lvclass"/>
			<Item Name="SequenceHandler.lvclass" Type="LVClass" URL="../../../main/Sequence/SequenceHandler/SequenceHandler.lvclass"/>
			<Item Name="FeedbackControlSequence.lvclass" Type="LVClass" URL="../../../main/Sequence/Concrete/FeedbackControlSequence/FeedbackControlSequence.lvclass"/>
			<Item Name="SequenceInputRunnerObjectAttributes.ctl" Type="VI" URL="../../../main/Sequence/SequenceInputRunner/SequenceInputRunnerObjectAttributes.ctl"/>
			<Item Name="SequenceInputRunner.lvclass" Type="LVClass" URL="../../../main/Sequence/SequenceInputRunner/SequenceInputRunner.lvclass"/>
			<Item Name="CRioCSeriesDeviceWriter.lvclass" Type="LVClass" URL="../../../drivers/DAQ/CRioCSeriesDeviceWriter/CRioCSeriesDeviceWriter.lvclass"/>
			<Item Name="XDNodeRunTimeDep.lvlib" Type="Library" URL="/&lt;vilib&gt;/Platform/TimedLoop/XDataNode/XDNodeRunTimeDep.lvlib"/>
			<Item Name="lvanlys.dll" Type="Document" URL="../../../../../Program Files (x86)/National Instruments/LabVIEW 2011/resource/lvanlys.dll"/>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="InputStream.lvlib" Type="Library" URL="../../../main/IOStreams/InputStream/InputStream.lvlib"/>
			<Item Name="Calibrate.lvclass" Type="LVClass" URL="../../../main/Calibration/Calibrate/Calibrate.lvclass"/>
			<Item Name="Threading.lvclass" Type="LVClass" URL="../../../main/Execution/Threading/Threading.lvclass"/>
			<Item Name="ExecutionBase.lvclass" Type="LVClass" URL="../../../main/Execution/ExecutionBase/ExecutionBase.lvclass"/>
			<Item Name="IO.lvclass" Type="LVClass" URL="../../../main/IOStreams/IO/IO.lvclass"/>
			<Item Name="url_decode.vi" Type="VI" URL="../../../main/Utilities/String/url_decode.vi"/>
			<Item Name="SetWaveformChannelNames.vi" Type="VI" URL="../../../main/Utilities/Waveform/SetWaveformChannelNames.vi"/>
			<Item Name="RegulatorObjectAttributes.ctl" Type="VI" URL="../../../main/ControlSystem/Regulator/RegulatorObjectAttributes.ctl"/>
			<Item Name="OutputStream.lvclass" Type="LVClass" URL="../../../main/IOStreams/OutputStream/OutputStream.lvclass"/>
			<Item Name="Controller.lvclass" Type="LVClass" URL="../../../main/ControlSystem/Controller/Controller.lvclass"/>
			<Item Name="ListFilesWithFileTypes.vi" Type="VI" URL="../../../main/Utilities/Path/ListFilesWithFileTypes.vi"/>
			<Item Name="WaitForOpen.vi" Type="VI" URL="../../../main/GUI/SubPanelUtilities/WaitForOpen.vi"/>
			<Item Name="UserEventType.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/UserEventType.ctl"/>
			<Item Name="SubPanelOutgoingData.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/SubPanelOutgoingData.ctl"/>
			<Item Name="OutgoingUserEvent.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/OutgoingUserEvent.ctl"/>
			<Item Name="BroadcastEventType.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/BroadcastEventType.ctl"/>
			<Item Name="BroadCastIncomingEvent.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/BroadCastIncomingEvent.ctl"/>
			<Item Name="SubPanelIncomingData.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/SubPanelIncomingData.ctl"/>
			<Item Name="IncomingUserEvent.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/IncomingUserEvent.ctl"/>
			<Item Name="PidRegulatorObjectAttributes.ctl" Type="VI" URL="../../../main/ControlSystem/Concrete/PIDRegulator/PidRegulatorObjectAttributes.ctl"/>
			<Item Name="LeadLag.vi" Type="VI" URL="../../../main/ControlSystem/Concrete/PIDRegulator/LeadLag.vi"/>
			<Item Name="Regulator.xctl" Type="XControl" URL="../../../main/ControlSystem/Regulator/XControl/Regulator/Regulator.xctl"/>
			<Item Name="PidRegulator.xctl" Type="XControl" URL="../../../main/ControlSystem/Concrete/PIDRegulator/XControl/PidRegulator.xctl"/>
			<Item Name="UnbufferedDeviceWriter.lvclass" Type="LVClass" URL="../../../main/IOStreams/UnbufferedDeviceWriter/UnbufferedDeviceWriter.lvclass"/>
			<Item Name="DeviceWriter.lvclass" Type="LVClass" URL="../../../main/IOStreams/DeviceWriter/DeviceWriter.lvclass"/>
			<Item Name="RingBuffer.lvlib" Type="Library" URL="../../../main/Buffer/RingBuffer/RingBuffer.lvlib"/>
			<Item Name="UnbufferedDeviceReader.lvclass" Type="LVClass" URL="../../../main/IOStreams/UnbufferedDeviceReader/UnbufferedDeviceReader.lvclass"/>
			<Item Name="DeviceReader.lvclass" Type="LVClass" URL="../../../main/IOStreams/DeviceReader/DeviceReader.lvclass"/>
			<Item Name="SubPanelInitiate.vi" Type="VI" URL="../../../main/GUI/SubPanelUtilities/SubPanelInitiate.vi"/>
			<Item Name="InitData.ctl" Type="VI" URL="../../../main/ControlSystem/Regulator/XControl/RegulatorConfiguration/InitData.ctl"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
