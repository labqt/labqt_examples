﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="14008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6982244</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">12124142</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,.!!!*Q(C=T:2.LF*"%)50;K)TQQIUR!W9GJIY91&amp;-G,C!=AE-'-C1,&gt;17#$NAA$KOM3^Z#1-X1"RKP!'`W`4DZ`W!!UV?8[IP^ZTK[I`OZEKFP:#?;8OJD=_W[F/N\=;\&lt;P&gt;UZ+NW/OW/RZ&gt;2WH?HY]@X$$WJ8Q=]Z*`DP^4?H'WX`=(AH$_9.`0ZY#%@NU5[S4DYV&lt;W6M@=0\C\DNN_=_*O\`PR19&lt;0:.0@Y.7/T?&gt;!H9\YZ[V@\]@HT%\^]06X`JNH\\=_M'=@\V_T])`@W`B&gt;D0$`9&gt;]Z0=_,?^3PJ`O(S_&lt;T1_/N-,P]*`G_4/GJ@2%1314BB;KVNIC&gt;[IC&gt;[IC&gt;[I!&gt;[I!&gt;[I!?[ITO[ITO[ITO[I2O[I2O[I2N[O&gt;#&amp;,H3BMSL*Z-F%3&gt;'E1*)-CJ)OY5FY%J[%BU=F0!F0QJ0Q*$SE+/&amp;*?"+?B#@B9:A3HI1HY5FY%BZ+&amp;:)M&amp;TI]#1`F&amp;@!%0!&amp;0Q"0Q-+5#HA!AG#QI("1"1Y%:@!FY!J[!B[]+?!+?A#@A#8CQ&amp;@!%0!&amp;0Q"0Q-+3M3B3;^E+(BT*S?"Q?B]@B=8AI,9@(Y8&amp;Y("[(B_HE]$A]$I1TI6-="$G$H!4HQ?&amp;R?,D*Y8&amp;Y("[(R_("+DPE:76;GP:#B]@A-8A-(I0(Y+'%$"[$R_!R?!Q?SMLA-8A-(I0(Y'%K'4Q'D]&amp;DA"C4-LW-9M:!)]E1$"Y_Z&lt;29W;5I*&amp;9OK2Z?V5/J?NB5$Z(KY6$&gt;&gt;.8.6.UEV=688646R6*&gt;".5@JQKN#K-[C?LA.F&amp;L_B7R)'&lt;%F"A21[*0^)BO/`1@*[\8;[V7+SU7#]VG-UWH5YV')QW(1`8\@@6[087\8&gt;WUVVS\VDF_,T5@XSZ`8X^&lt;.F_\S_W(K_80[[PFD4=BN]44TZX*SR_&gt;S@OGQ]#`D?_44^N@EV@=HW_`4$JI?F@K\&gt;Z,D_(&gt;K#@;X@&gt;L^!=6!JH-!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"P$5F.31QU+!!.-6E.$4%*76Q!!&amp;U!!!!3$!!!!)!!!&amp;S!!!!!D!!!!!2Z4&gt;'&amp;S&gt;'6S3WFU5H"D1WRJ:7ZU18"J,GRW9WRB=X-!!!!!E"1!A!!!-!!!+!!!!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!"V&lt;ZZ^&amp;O!73LL'(Y)0PE(Z!!!!$!!!!"!!!!!!855V"E!&gt;:U/,:;BO9PO"J.1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!(=[I&amp;%MVX&amp;)K52KA5BX/9I"!!!!`````Q!!!"$QP&gt;V!I,A1%M&lt;+$]-0V]ZK!!!!"!!!!!!!!!%$!!&amp;-6E.$!!!!!Q!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF$1Q!!!!!"&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!&amp;"53$!!!!!&lt;!!%!!Q!!&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!!!!!A!"`Q!!!!%!!1!!!!!!!A!!!!!!!!!!!!!!!!!!!!!!!F:*5%E!!!!!!!)85X2B=H2F=ENJ&gt;%.P:'6D,GRW9WRB=X-#"Q!!5&amp;2)-!!!!#]!!1!&amp;!!!!$V.U98*U:8*,;82$&lt;W2F9R&gt;4&gt;'&amp;S&gt;'6S3WFU1W^E:7-O&lt;(:D&lt;'&amp;T=Q!!!!!!!!%!!!!!!!)!!1!!!!!#!!!!!Q!!!!!#!!)!!!!!!#1!!!!??*RDY'2A&lt;G#YQ!$%D!R-$5Q;1.9("A9/-'1!!&amp;?0"/)!!!")!!!"'(C=9W$!"0_"!%AR-D!Q81$3,'DC9"L'JC&lt;!:3YOO[$CT&amp;!XMM+%A?\?![3:1(*1.6!`-*U"YB0IZP"$[2F)9A#(MSD&amp;!!!!$!!"6EF%5Q!!!!!!!Q!!!;1!!!,U?*S.5D&amp;,QV!1PP?-_M2)!B9*YK!93B52R3)&gt;/F3;1I?#&amp;=7BAYM+&amp;11&gt;($IY6&amp;K(%"U%@Y1`1+1YJ;31\CY/,O+31;?OAN[^*+6$"1`SPH@X\LP\\O5&amp;$/"E)\.:ZA!DO%^!(1\0DI[H!(U)\9V$X`)2LLO?R&amp;U7_IV0V&lt;&amp;-P8&amp;J'FRL*D(C&amp;%`^L3S&gt;/=*74.]S"4G)#O(04L8254WH:#&lt;MEKFA2C$L9/R$VN/R8M+JV\#"D6TUZB%6R,GQH%LZQ;1E[;4G2@)-G5(=%D*+JIZ\&amp;6%-NLFW.=NN&gt;!T0C_9J)UP5;YOA.25Z8M8T[_FIO*B6]710[/M&lt;JCA?.*C)F&gt;'^`F`8P;M^&gt;6P573OU33,U:$_"0,X^LDB8;`A`&lt;O^IR:"K\QH$N]4+$';R,AGSV@AOB:?02NCH!B@9.%V#&amp;9L+('@-T[5S[0KZB*"1@:"1+^)MO@-!P29N7/X!AX,U,J:BO.XAFQ5G.8]B,C&amp;T&amp;&gt;&amp;"\#'_MP$]'T'&amp;3%,')8RA$!1]MG=WSF5_T7@Z!E`S0^L]S`,&lt;6G(1D^`R,[9XE0-!!!!4!!!!#8C=9W"A9'2E!!)!!"1!!Q!!!!!/&amp;!'!*Q!!"D%U,D!O-1!!!!!!!!Q5!)!!!!!%-41O-!!!!!!/&amp;!'!*Q!!"D%U,D!O-1!!!!!!!!Q5!)!!!!!%-41O-!!!!!!/&amp;!'!*Q!!"D%U,D!O-1!!!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0```````````'=RP`P;VL`]QB'``VL8P`D;V\```````````Y!!!!'!!!!"A!0!!9!0]!'!0`Q"A0``!9$``Q'!``]"A0``!9$``Q'!``]"A0``!9$``Q'!``]"A0``!9$``_'!@``ZA"``Y9!(`]'!!@]"A!!]!9!!!!(`````!!!#!0`````````````````````^X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X@`&gt;X&gt;!!X1$&gt;U!X1!.U.X&gt;X`X&gt;U.X&gt;$&gt;$1X1U.U.$&gt;X&gt;`^X&gt;U!X1!!U!!.!!X1X&gt;X@`&gt;X&gt;X1U.U.$&gt;$1X&gt;U.X&gt;X`X&gt;U!$&gt;$&gt;$1X1U.X&gt;$&gt;X&gt;`^X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X@``````````````````````!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!$-!!!!!!!!!0]!!!!!!!$0``Q!!!!!!!$`!!!!!!$0`&gt;X@`!!!!!!!`Q!!!!$0`&gt;X&gt;X&gt;`]!!!!!0]!!!!0`&gt;X&gt;X&gt;X&gt;X`!!!!$`!!!!$`X&gt;X&gt;X&gt;X&gt;`Q!!!!`Q!!!!```&gt;X&gt;X&gt;``]!!!!0]!!!!0```^X&gt;````!!!!$`!!!!$``````````Q!!!!`Q!!!!``````````]!!!!0]!!!!0``````````!!!!$`!!!!$``````````Q!!!!`Q!!!!``````````]!!!!0]!!!!0``````````!!!!$`!!!!$````````````Q!!`Q!!!!$`````````````!0]!!!!!!0``````````!!$`!!!!!!!!```^````]!!!`Q!!!!!!!!$^````]!!!!0]!!!!!!!!!!!``]!!!!!$`!!!!!!!!!!!!!!!!!!!!``````````````````````!!!%!0```````````````````````````````````````````XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?```?XN\?XM!!!"\?Q!!?XN\!!"\?Q!!!(N\!(N\?XN\``^\?XN\!(N\?XM!?XM!?Q"\?Q"\!(N\!(M!?XN\?XP``XN\?XN\!!"\?Q!!!!"\!!!!!(M!!!"\?Q"\?XN\?```?XN\?XN\?Q"\!(N\!(M!?XM!?Q"\?XN\!(N\?XN\``^\?XN\!!!!?XM!?XM!?Q"\?Q"\!(N\?XM!?XN\?XP``XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?`````````````````````````````````````````````]E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#4``S1E*#1E*#1E*#1E*#1E5&amp;!E*#1E*#1E*#1E*#1E*0``*#1E*#1E*#1E*#1E5+T]L+R1*#1E*#1E*#1E*#1E``]E*#1E*#1E*#1E5+T]?XN\?[SM5#1E*#1E*#1E*#4``S1E*#1E*#1E5+T]?XN\?XN\?XOML&amp;!E*#1E*#1E*0``*#1E*#1E*+T]?XN\?XN\?XN\?XN\L+QE*#1E*#1E``]E*#1E*#1E`0R\?XN\?XN\?XN\?XP_L#1E*#1E*#4``S1E*#1E*#4]L+T]?XN\?XN\?XP_`P\]*#1E*#1E*0``*#1E*#1E*0SML+SM`(N\?XP_`P\_`PQE*#1E*#1E``]E*#1E*#1E`+SML+SML0SM`P\_`P\_`#1E*#1E*#4``S1E*#1E*#4]L+SML+SML0\_`P\_`P\]*#1E*#1E*0``*#1E*#1E*0SML+SML+SM`P\_`P\_`PQE*#1E*#1E``]E*#1E*#1E`+SML+SML+T_`P\_`P\_`#1E*#1E*#4``S1E*#1E*#4]L+SML+SML0\_`P\_`P\]*#1E*#1E*0``*#1E*#1E*0SML+SML+SM`P\_`P\_`PQE*#1E*#1E``]E*#1E*#1EL+SML+SML+T_`P\_`P[ML+SML#1E*#4``S1E*#1E*#1E`0SML+SML0\_`P[ML0SML+SML+QE*0``*#1E*#1E*#1E*0SML+SM`P[ML0SML+SML+QE*#1E``]E*#1E*#1E*#1E*#4]L+SML(OML+SML+SM*#1E*#4``S1E*#1E*#1E*#1E*#1E`(OML+SML+SM*#1E*#1E*0``*#1E*#1E*#1E*#1E*#1E*#3ML+SM*#1E*#1E*#1E``]E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#4```````````````````````````````````````````]!!!!#!!)!!!!!!99!!5:13&amp;!!!!!$!!*'5&amp;"*!!!!!B&gt;4&gt;'&amp;S&gt;'6S3WFU1W^E:7-O&lt;(:D&lt;'&amp;T=Q)(!!"16%AQ!!!!,Q!"!!5!!!!05X2B=H2F=ENJ&gt;%.P:'6D&amp;V.U98*U:8*,;82$&lt;W2F9SZM&gt;G.M98.T!!!!!!!!!1!!!!!!!A!"!!!!!!)!!!!!!!!!!!%!!!"9!!*52%.$!!!!!!!"&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!&amp;"53$!!!!!&lt;!!%!!Q!!&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!!!!!A!!`Q!!!!%!!1!!!!!!!A!!!!!!!!!!!!!!!!!!!!!!!!!"!!!!#V"53$!!!!!!!!!!!!!#2%2131!!!!!!!B&gt;4&gt;'&amp;S&gt;'6S3WFU1W^E:7-O&lt;(:D&lt;'&amp;T=Q)(!!"16%AQ!!!!,Q!"!!5!!!!05X2B=H2F=ENJ&gt;%.P:'6D&amp;V.U98*U:8*,;82$&lt;W2F9SZM&gt;G.M98.T!!!!!!!!!1!!!!!!!A!"!!!!!!)!!!!!!!!!!!%!!!!K!!-!!!!!!]!!!!FE?*SNFEVI%U%9BL`:LD+J&amp;C?V;A/7R,#N9CX5@SX_R5\6NFJN9[#N,2K&lt;K"6N.&gt;GK*U7)1A`CI&gt;#$U)/88HPI19]CQ=N[[%%]_%0Q)IC)(B1&amp;OVG`W73TW;4&gt;8.S&amp;:6HGH7`G?Z^X&gt;Q%]&amp;VC^F)7\/B$W#W^/[F!&gt;UQB!OI6#`GB[##R#`A+J]R%&gt;DN!)?S^FS59&gt;6M?U*NL+*_%(DD;?'NPA$&lt;H#0O(1F=S(EV8LM#;G&lt;@"W+2GGT'V5*F&gt;9MXKBA4UC7;F@]@_GDV,D7""37]46WU+S10AG75Y&amp;TU4(YCF&amp;007U5*]ZJ5=(RL7;B*,:D$.C[:@GF&amp;)T#5I(L#E"J^Q#]`0TNMC&lt;%T7:S^C('B)%)$OF:B&gt;.,&gt;=;EEJGG[GJ.D69:[&gt;6B`OT5W,N1F1K8=MVBF,5X=FP77ANH8ZE3&gt;U6(?K5T&amp;(KIZ^C\UB.&lt;_I:%#$J]^2Y&lt;NQ3?G_0=-%=7YN'.,92&amp;M&lt;\M!Z&lt;5ZJU!73LP2-Q:&gt;IA7T9=1"P)-&gt;-'$Z^E90MQ6=%(,K?$$?X8*J*K0"%9PR19O2:.*A-X%K/XIGI]%)OKU8+($H,.MV@M8B1TY9"V)-..C":X?RRG:W?R!8CVJ9&gt;1OE(*&amp;(2?=T@R1M&gt;D&gt;O&gt;%6&lt;NTB\&amp;T`-`:S[*\D7W3E^F6.L-\E&amp;EP+VW7A^H&gt;`Z`:09D3X2*G92C'9-S&amp;P\UZE=UMDA?Y$M-OGHWIO6@-,'K'54.7G&gt;H^:=Q+&lt;1GTU^04J=SW&amp;:C6#=ERGVIQ&amp;IV&amp;1?YXYRP=BDN&amp;Z-&lt;1$E5,C$@'&gt;ST2D36%^[OM\L&gt;&lt;K0)Z@Z?4V5(X`A&gt;71IZ8&gt;PLCV@C)'F,6R/D&amp;#47?,$%%&amp;]V.8'K[]U]Q1BWZ#0'-PUN58*P@PC.'RQ5:09Y9Y@\!Q0Z62%-*1DE8*]K4)O0:!1-6E^*:EB2R&gt;.J_,:]5*0NIYQ!U$I+&gt;FSJH8PLNP*RSZ-6;H#-PJ`^`8MYMF:=)B/#3#`O^:8E*I;I0)C[;PL+]B,"/HV8(,3`B]LS%L&amp;JO?4F&lt;[0_SK@FI@)6$=.6/$6G.JH#N&amp;;S0\2&gt;D%&lt;:#LR0017(%EG`Z3)8ES0HEL!GLU13_[&lt;N(V@&lt;R7(SEX*FT8+P@LG3C7#,P&gt;1-S=2!5FS90==W(F(OM&amp;XI)7KUW`4SRX+&gt;QO!D4GPMZ4'8%F0I]057V[0N8%V4U4:SZ53NQV)/[VR^W.8^_P.2I[\_'BB6OO!VA(&lt;C5?TKMY_WV,VCH-).WUAYWAY:A3N&lt;4'@IL`&lt;&lt;QZZ-_76#G&amp;K1GC#N0`,4_(Z-^L?)!!!!%!!!!0!!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!1#!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)5!)!!!!!!!1!)!$$`````!!%!!!!!!,Q!!!!$!$*!=!!?!!!:&amp;V.U98*U:8*,;82$&lt;W2F9SZM&gt;G.M98.T!!^4&gt;'&amp;S&gt;'6S3WFU1W^E:7-!7A$RT.QU+Q!!!!)?5X2B=H2F=ENJ&gt;&amp;*Q9U.M;76O&gt;%&amp;Q;3ZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"Z!5!!"!!!14W*K:7.U182U=GFC&gt;82F=Q!!+%"1!!%!!2Z4&gt;'&amp;S&gt;'6S3WFU5H"D1WRJ:7ZU18"J,GRW9WRB=X-!!!%!!A!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)5!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!)!!!!!!!!!!1!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ&amp;!#!!!!!!!%!"1!(!!!"!!$-X$1_!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N="1!A!!!!!!"!!5!"Q!!!1!!T.QU0A!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-5!)!!!!!!!1!)!$$`````!!%!!!!!!,Q!!!!$!$*!=!!?!!!:&amp;V.U98*U:8*,;82$&lt;W2F9SZM&gt;G.M98.T!!^4&gt;'&amp;S&gt;'6S3WFU1W^E:7-!7A$RT.QU+Q!!!!)?5X2B=H2F=ENJ&gt;&amp;*Q9U.M;76O&gt;%&amp;Q;3ZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"Z!5!!"!!!14W*K:7.U182U=GFC&gt;82F=Q!!+%"1!!%!!2Z4&gt;'&amp;S&gt;'6S3WFU5H"D1WRJ:7ZU18"J,GRW9WRB=X-!!!%!!A!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF&amp;!#!!!!!!!%!"1!$!!!"!!!!!!!%!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U921!A!!!!!!$!$*!=!!?!!!:&amp;V.U98*U:8*,;82$&lt;W2F9SZM&gt;G.M98.T!!^4&gt;'&amp;S&gt;'6S3WFU1W^E:7-!7A$RT.QU+Q!!!!)?5X2B=H2F=ENJ&gt;&amp;*Q9U.M;76O&gt;%&amp;Q;3ZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"Z!5!!"!!!14W*K:7.U182U=GFC&gt;82F=Q!!+%"1!!%!!2Z4&gt;'&amp;S&gt;'6S3WFU5H"D1WRJ:7ZU18"J,GRW9WRB=X-!!!%!!A!!!!%:&amp;V.U98*U:8*,;82$&lt;W2F9SZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!!!!!!!%!!5!$!!!!!1!!!"&gt;!!!!+!!!!!)!!!1!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!&amp;"!!!#PXC=J6("3M.!%(X*VD&lt;6;KN7;QO7Y+%(B2\5M[1520"AK4&gt;0JMF')IM*S&lt;2Y^!0^"$^#`]$:&lt;;21+28EQ?T/P.WX-W]"&gt;($OJ?A#\&gt;9^_2H*\$;G92,+I+^GA@,T(05F!A`Y?P_Y0!.A&gt;R@=/!W'+J9P.%DDH\P.O]GT$'B!F-74+=G](Z"#VRP"!BL,*+#-]N5@F$ML;+V`;P3NY[';ZHT%43,8X((4,*\Z*.X1*R]&lt;$'QC"+R0#%Z%E?+#E[J8A9D5%RTP432DGB&gt;E%K("[A)6/+C+;2DB"'P]Q]U``;J"T\/&amp;GJGL*OAVZ%Z(M$FOI\03T:ZB[GN^M-'BL!0L/(DE2EOYZA&gt;=\'+0B^89.Y&lt;.)1L]LCS9JM(=5!M(240L0K5%X9%:#!W/.F@+W/(^)6KG?M2L#WWN_AU$+[0.!!!!!!!!:1!"!!)!!Q!%!!!!3!!0"!!!!!!0!.A!V1!!!&amp;%!$Q1!!!!!$Q$9!.5!!!";!!]%!!!!!!]!W!$6!!!!9Y!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!&amp;U!!!!3$!!!!)!!!&amp;S!!!!!!!!!!!!!!!#!!!!!U!!!%;!!!!"Z-35*/!!!!!!!!!8B-6F.3!!!!!!!!!9R36&amp;.(!!!!!!!!!;"$1V.5!!!!!!!!!&lt;2-38:J!!!!!!!!!=B$4UZ1!!!!!!!!!&gt;R544AQ!!!!!!!!!@"%2E24!!!!!!!!!A2-372T!!!!!!!!!BB735.%!!!!!!!!!CR(1U2*!!!!!!!!!E"W:8*T!!!!"!!!!F241V.3!!!!!!!!!LB(1V"3!!!!!!!!!MR*1U^/!!!!!!!!!O"J9WQU!!!!!!!!!P2J9WQY!!!!!!!!!QB$5%-S!!!!!!!!!RR-37:Q!!!!!!!!!T"'5%BC!!!!!!!!!U2'5&amp;.&amp;!!!!!!!!!VB75%21!!!!!!!!!WR-37*E!!!!!!!!!Y"#2%BC!!!!!!!!!Z2#2&amp;.&amp;!!!!!!!!![B73624!!!!!!!!!\R%6%B1!!!!!!!!!^".65F%!!!!!!!!!_2)36.5!!!!!!!!!`B71V21!!!!!!!!"!R'6%&amp;#!!!!!!!!"#!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!0````]!!!!!!!!!P!!!!!!!!!!!`````Q!!!!!!!!$1!!!!!!!!!!$`````!!!!!!!!!.A!!!!!!!!!!0````]!!!!!!!!"Y!!!!!!!!!!!`````Q!!!!!!!!(I!!!!!!!!!!$`````!!!!!!!!!B!!!!!!!!!!!0````]!!!!!!!!#8!!!!!!!!!!!`````Q!!!!!!!!*M!!!!!!!!!!$`````!!!!!!!!""1!!!!!!!!!"0````]!!!!!!!!%,!!!!!!!!!!(`````Q!!!!!!!!2!!!!!!!!!!!D`````!!!!!!!!"&amp;!!!!!!!!!!#@````]!!!!!!!!%:!!!!!!!!!!+`````Q!!!!!!!!2U!!!!!!!!!!$`````!!!!!!!!")A!!!!!!!!!!0````]!!!!!!!!%I!!!!!!!!!!!`````Q!!!!!!!!3U!!!!!!!!!!$`````!!!!!!!!"4A!!!!!!!!!!0````]!!!!!!!!(0!!!!!!!!!!!`````Q!!!!!!!!N!!!!!!!!!!!$`````!!!!!!!!#UA!!!!!!!!!!0````]!!!!!!!!-V!!!!!!!!!!!`````Q!!!!!!!"#9!!!!!!!!!!$`````!!!!!!!!%+!!!!!!!!!!!0````]!!!!!!!!1K!!!!!!!!!!!`````Q!!!!!!!"#Y!!!!!!!!!!$`````!!!!!!!!%3!!!!!!!!!!!0````]!!!!!!!!2+!!!!!!!!!!!`````Q!!!!!!!"5Q!!!!!!!!!!$`````!!!!!!!!&amp;4A!!!!!!!!!!0````]!!!!!!!!61!!!!!!!!!!!`````Q!!!!!!!"6M!!!!!!!!!)$`````!!!!!!!!&amp;L1!!!!!'F.U98*U:8*,;823='.$&lt;'FF&lt;H2"='EO9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!2Z4&gt;'&amp;S&gt;'6S3WFU5H"D1WRJ:7ZU18"J,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!!!!1!"!!!!!!!#!!!!!!-!-E"Q!"Y!!"E85X2B=H2F=ENJ&gt;%.P:'6D,GRW9WRB=X-!$V.U98*U:8*,;82$&lt;W2F9Q";!0(-X$1L!!!!!BZ4&gt;'&amp;S&gt;'6S3WFU5H"D1WRJ:7ZU18"J,GRW9WRB=X-54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!(E"1!!%!!""09GJF9X2"&gt;(2S;7*V&gt;'6T!!"M!0(-X$1_!!!!!BZ4&gt;'&amp;S&gt;'6S3WFU5H"D1WRJ:7ZU18"J,GRW9WRB=X-;5X2B=H2F=ENJ&gt;&amp;*Q9U.M;76O&gt;%&amp;Q;3ZD&gt;'Q!+E"1!!%!!2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!)!!!!"`````Q!!!!%:&amp;V.U98*U:8*,;82$&lt;W2F9SZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!!!"%6*Q9U.M;76O&gt;#ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!"%!A!A!!!!!!!!!!!!%!!!!&amp;6.Z&lt;7*P&gt;%6D;'^"5%EO&lt;(:D&lt;'&amp;T=Q!!!"F4?7VC&lt;X2"9X2V982P=E&amp;133ZM&gt;G.M98.T!!!!&amp;6.Z&lt;7*P&gt;%&amp;135*B=W5O&lt;(:D&lt;'&amp;T=Q!!!"F4&gt;'&amp;S&gt;'6S3WFU16"*1G&amp;T:3ZM&gt;G.M98.T</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"=!!!!!2&amp;3='.$&lt;'FF&lt;H1O&lt;(:D&lt;'&amp;T=V"53$!!!!!_!!%!#1!!!!!!"'VB;7Y45G6N&lt;X2F5(*P9W6E&gt;8*F1W&amp;M&lt;!F3='.$&lt;'FF&lt;H125H"D1WRJ:7ZU,GRW9WRB=X-!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="StarterKitRpcClientApi.ctl" Type="Class Private Data" URL="StarterKitRpcClientApi.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="IO" Type="Folder">
		<Item Name="Read.vi" Type="VI" URL="../Read.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!1!!!!!1!)!0)(&gt;]_@!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="ReadString.vi" Type="VI" URL="../ReadString.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!-0````]*5X2S;7ZH)'FO!$R!=!!?!!!A(F.U98*U:8*,;823='.$&lt;'FF&lt;H2"='EO&lt;(:D&lt;'&amp;T=Q!!%6.Z&lt;7*P&gt;%&amp;135*B=W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!0%"Q!"Y!!#!?5X2B=H2F=ENJ&gt;&amp;*Q9U.M;76O&gt;%&amp;Q;3ZM&gt;G.M98.T!!!15XFN9G^U16"*1G&amp;T:3"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082663440</Property>
		</Item>
		<Item Name="Write.vi" Type="VI" URL="../Write.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!1!!!!!1!)!0)(&gt;B";!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="WriteInt.vi" Type="VI" URL="../WriteInt.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;0!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!A(F.U98*U:8*,;823='.$&lt;'FF&lt;H2"='EO&lt;(:D&lt;'&amp;T=Q!!%6.Z&lt;7*P&gt;%&amp;135*B=W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$5!$!!:*&lt;H1A;7Y!!$R!=!!?!!!A(F.U98*U:8*,;823='.$&lt;'FF&lt;H2"='EO&lt;(:D&lt;'&amp;T=Q!!%&amp;.Z&lt;7*P&gt;%&amp;135*B=W5A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!I!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="WriteDbl.vi" Type="VI" URL="../WriteDbl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!A(F.U98*U:8*,;823='.$&lt;'FF&lt;H2"='EO&lt;(:D&lt;'&amp;T=Q!!%6.Z&lt;7*P&gt;%&amp;135*B=W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%5!+!!N%&lt;X6C&lt;'5W.#"J&lt;A!]1(!!(A!!)"Z4&gt;'&amp;S&gt;'6S3WFU5H"D1WRJ:7ZU18"J,GRW9WRB=X-!!""4?7VC&lt;X2"5%F#98.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!+!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="WriteString.vi" Type="VI" URL="../WriteString.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!A(F.U98*U:8*,;823='.$&lt;'FF&lt;H2"='EO&lt;(:D&lt;'&amp;T=Q!!%6.Z&lt;7*P&gt;%&amp;135*B=W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%E!Q`````QF4&gt;(*J&lt;G=A;7Y!0%"Q!"Y!!#!?5X2B=H2F=ENJ&gt;&amp;*Q9U.M;76O&gt;%&amp;Q;3ZM&gt;G.M98.T!!!15XFN9G^U16"*1G&amp;T:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#A!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1118306832</Property>
		</Item>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		<Item Name="StarterKitRpcClientApi_Create.vi" Type="VI" URL="../StarterKitRpcClientApi_Create.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&gt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!A(F.U98*U:8*,;823='.$&lt;'FF&lt;H2"='EO&lt;(:D&lt;'&amp;T=Q!!%6.Z&lt;7*P&gt;%&amp;135*B=W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+%"Q!"Y!!"=628BF9X6U;7^O1G&amp;T:3ZM&gt;G.M98.T!!:198*F&lt;H1!!$R!=!!?!!!A(F.U98*U:8*,;823='.$&lt;'FF&lt;H2"='EO&lt;(:D&lt;'&amp;T=Q!!%&amp;.Z&lt;7*P&gt;%&amp;135*B=W5A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!I!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107825168</Property>
		</Item>
		<Item Name="RPC_Request.vi" Type="VI" URL="../RPC_Request.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!A(F.U98*U:8*,;823='.$&lt;'FF&lt;H2"='EO&lt;(:D&lt;'&amp;T=Q!!%6.Z&lt;7*P&gt;%&amp;135*B=W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$U!$!!F$&lt;WVN97ZE371!0%"Q!"Y!!#!?5X2B=H2F=ENJ&gt;&amp;*Q9U.M;76O&gt;%&amp;Q;3ZM&gt;G.M98.T!!!15XFN9G^U16"*1G&amp;T:3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!*)!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="RPC_IO.vi" Type="VI" URL="../RPC_IO.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%V!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!A(F.U98*U:8*,;823='.$&lt;'FF&lt;H2"='EO&lt;(:D&lt;'&amp;T=Q!!%6.Z&lt;7*P&gt;%&amp;135*B=W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!0%"Q!"Y!!#!?5X2B=H2F=ENJ&gt;&amp;*Q9U.M;76O&gt;%&amp;Q;3ZM&gt;G.M98.T!!!15XFN9G^U16"*1G&amp;T:3"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="Destroy.vi" Type="VI" URL="../Destroy.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%L!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$R!=!!?!!!A(F.U98*U:8*,;823='.$&lt;'FF&lt;H2"='EO&lt;(:D&lt;'&amp;T=Q!!%6.Z&lt;7*P&gt;%&amp;135*B=W5A&lt;X6U!":!5!!$!!!!!1!##'6S=G^S)'FO!!!]1(!!(A!!)"Z4&gt;'&amp;S&gt;'6S3WFU5H"D1WRJ:7ZU18"J,GRW9WRB=X-!!""4?7VC&lt;X2"5%F#98.F)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!)E!!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777352</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1084760592</Property>
		</Item>
	</Item>
</LVClass>
