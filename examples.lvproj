﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="varPersistentID:{068201F4-C487-478E-A9FB-DEBB403C9679}" Type="Ref">/My Computer/examples/Sequence/SequenceData.lvlib/CurrentState:1</Property>
	<Property Name="varPersistentID:{1CF1BE87-EE68-4C77-B43B-E6764CE356F1}" Type="Ref">/My Computer/examples/Sequence/SequenceLogic.lvlib/NextState:1</Property>
	<Property Name="varPersistentID:{285C1EE0-E08B-4DF3-8F55-FABEFCA6E8A7}" Type="Ref">/My Computer/examples/Generics/Implementations/libraries/DigitalOutput.lvlib/HeaterOut_2</Property>
	<Property Name="varPersistentID:{34B9C284-08E3-4B9D-B6E2-8C2445ABF021}" Type="Ref">/My Computer/examples/Generics/Implementations/libraries/DigitalOutput.lvlib/HeaterOut_1</Property>
	<Property Name="varPersistentID:{45AFAAFA-02AB-43A1-B55D-11E6465E8EF7}" Type="Ref">/My Computer/examples/Sequence/SequenceData.lvlib/StateTime:1</Property>
	<Property Name="varPersistentID:{466B30E0-418B-4509-A734-6E64FC6420C3}" Type="Ref">/My Computer/examples/Sequence/SequenceData.lvlib/CurrentStep:1</Property>
	<Property Name="varPersistentID:{50F4FEF7-A657-4F6D-A8B4-50C1D9838557}" Type="Ref">/My Computer/examples/Sequence/Criterias.lvlib/ForceCriteria:1</Property>
	<Property Name="varPersistentID:{65EB2540-6A7C-4318-B3C7-2099A609E2DE}" Type="Ref">/My Computer/examples/Sequence/SequenceLogic.lvlib/Criteria:1</Property>
	<Property Name="varPersistentID:{6A483409-6F11-4EE3-8525-94A591D4125B}" Type="Ref">/My Computer/examples/Sequence/TestData.lvlib/Force:2</Property>
	<Property Name="varPersistentID:{761EF5FD-03BA-4F62-AF65-A5F73FFFFD39}" Type="Ref">/My Computer/examples/Sequence/TestData.lvlib/Pressure:1</Property>
	<Property Name="varPersistentID:{7EEE938F-7148-49E9-B0DE-7DF4C24BD3A7}" Type="Ref">/My Computer/examples/Sequence/SequenceLogic.lvlib/Pause:1</Property>
	<Property Name="varPersistentID:{8E0BCA22-CDDB-4DA5-8161-2E8F128C0D8D}" Type="Ref">/My Computer/examples/Sequence/Criterias.lvlib/SpeedCriteria:1</Property>
	<Property Name="varPersistentID:{BA7D78E1-92C6-42E3-B86E-64FDA98E7294}" Type="Ref">/My Computer/examples/Sequence/Criterias.lvlib/PressureCriteria:1</Property>
	<Property Name="varPersistentID:{E7017F98-81D6-4A81-9B6F-D97540F3FC91}" Type="Ref">/My Computer/examples/Sequence/TestData.lvlib/Speed:1</Property>
	<Property Name="varPersistentID:{EA658340-C285-4E24-AC75-C68B5BD44C6C}" Type="Ref">/My Computer/examples/Sequence/TestData.lvlib/Force:1</Property>
	<Property Name="varPersistentID:{FCE5141C-63C0-40AA-83BF-A9E5D7C11B7A}" Type="Ref">/My Computer/examples/Robotics/Classes/Misc/Measurements.lvlib/Measurements</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="examples" Type="Folder" URL="..">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="niDCPower Abort.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Abort.vi"/>
				<Item Name="niDCPower Close.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Close.vi"/>
				<Item Name="niDCPower Configure Current Level.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Current Level.vi"/>
				<Item Name="niDCPower Configure Output Enabled.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Output Enabled.vi"/>
				<Item Name="niDCPower Configure Output Function.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Output Function.vi"/>
				<Item Name="niDCPower Configure Sense.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Sense.vi"/>
				<Item Name="niDCPower Configure Voltage Level.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Voltage Level.vi"/>
				<Item Name="niDCPower Initialize With Channels.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Initialize With Channels.vi"/>
				<Item Name="niDCPower Initiate.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Initiate.vi"/>
				<Item Name="niDCPower IVI Error Converter.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower IVI Error Converter.vi"/>
				<Item Name="niDCPower Measure Multiple.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Measure Multiple.vi"/>
				<Item Name="niDCPower Output Function.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Output Function.ctl"/>
				<Item Name="niDCPower Self Test.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Self Test.vi"/>
				<Item Name="niDCPower Sense.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Sense.ctl"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="_simSolvers.ctl" Type="VI" URL="/&lt;vilib&gt;/Simulation/Scripting/Companion Diagram/SimConfigNode/_simSolvers.ctl"/>
				<Item Name="Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Analog to Digital.vi"/>
				<Item Name="Append Waveforms.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Append Waveforms.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Built App File Layout.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/Built App File Layout.vi"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="CD State Feedback Controller (Single Output).vi" Type="VI" URL="/&lt;vilib&gt;/addons/Control Design/_Implementation/CD State Feedback Controller (Single Output).vi"/>
				<Item Name="CD State Feedback Controller.vi" Type="VI" URL="/&lt;vilib&gt;/addons/Control Design/_Implementation/CD State Feedback Controller.vi"/>
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="Check for Equality.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for Equality.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create ActiveX Event Queue.vi"/>
				<Item Name="Create Directory Recursive.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Create Directory Recursive.vi"/>
				<Item Name="Create Error Clust.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create Error Clust.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="DAQmx Clear Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Flatten Channel String.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Flatten Channel String.vi"/>
				<Item Name="DAQmx Is Task Done.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Is Task Done.vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Read (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Read (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I8).vi"/>
				<Item Name="DAQmx Read (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I16).vi"/>
				<Item Name="DAQmx Read (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I32).vi"/>
				<Item Name="DAQmx Read (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U8).vi"/>
				<Item Name="DAQmx Read (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U16).vi"/>
				<Item Name="DAQmx Read (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U32).vi"/>
				<Item Name="DAQmx Read.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read.vi"/>
				<Item Name="DAQmx Start Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Start Task.vi"/>
				<Item Name="DAQmx Stop Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Stop Task.vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1DTicks NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1DTicks NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Frequency 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Frequency 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Write (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Write (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I8).vi"/>
				<Item Name="DAQmx Write (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I16).vi"/>
				<Item Name="DAQmx Write (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I32).vi"/>
				<Item Name="DAQmx Write (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U8).vi"/>
				<Item Name="DAQmx Write (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U16).vi"/>
				<Item Name="DAQmx Write (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U32).vi"/>
				<Item Name="DAQmx Write.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write.vi"/>
				<Item Name="Destroy ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Destroy ActiveX Event Queue.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Digital Size.vi"/>
				<Item Name="Digital to Analog.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Digital to Analog.vi"/>
				<Item Name="Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Digital to Boolean Array.vi"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="DTbl Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Analog to Digital.vi"/>
				<Item Name="DTbl Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Compress Digital.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DTbl Digital to Analog.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital to Analog.vi"/>
				<Item Name="DTbl Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital to Boolean Array.vi"/>
				<Item Name="DTbl Invert Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Invert Digital.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DWDT Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Analog to Digital.vi"/>
				<Item Name="DWDT Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital Size.vi"/>
				<Item Name="DWDT Digital to Analog.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital to Analog.vi"/>
				<Item Name="DWDT Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital to Boolean Array.vi"/>
				<Item Name="DWDT Empty Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Empty Digital.vi"/>
				<Item Name="DWDT Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Error Code.vi"/>
				<Item Name="DWDT Invert Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Invert Digital.vi"/>
				<Item Name="DWDT Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Uncompress Digital.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="EventData.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/EventData.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="FxpSim.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/FXPMathLib/sim/FxpSim.dll"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Generate Temporary File Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Generate Temporary File Path.vi"/>
				<Item Name="Generate Value.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/Generate Value.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="GOOP Object Repository Method.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository Method.ctl"/>
				<Item Name="GOOP Object Repository Statistics.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository Statistics.ctl"/>
				<Item Name="GOOP Object Repository.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/_goopsup.llb/GOOP Object Repository.vi"/>
				<Item Name="Handle Open Word or Excel File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Toolkit/Handle Open Word or Excel File.vi"/>
				<Item Name="Image Type" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Image Type"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="IMAQ ArrayToImage" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ ArrayToImage"/>
				<Item Name="IMAQ Clear Overlay" Type="VI" URL="/&lt;vilib&gt;/vision/Overlay.llb/IMAQ Clear Overlay"/>
				<Item Name="IMAQ Copy" Type="VI" URL="/&lt;vilib&gt;/vision/Management.llb/IMAQ Copy"/>
				<Item Name="IMAQ Create" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Create"/>
				<Item Name="IMAQ Dispose" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Dispose"/>
				<Item Name="IMAQ Image.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/IMAQ Image.ctl"/>
				<Item Name="IMAQ ImageToArray" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ ImageToArray"/>
				<Item Name="IMAQ Overlay Oval" Type="VI" URL="/&lt;vilib&gt;/vision/Overlay.llb/IMAQ Overlay Oval"/>
				<Item Name="IMAQ ReadFile" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ ReadFile"/>
				<Item Name="IMAQdx.ctl" Type="VI" URL="/&lt;vilib&gt;/userdefined/High Color/IMAQdx.ctl"/>
				<Item Name="Invert Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Invert Digital.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVFixedPointOverflowPolicyTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/fxp/LVFixedPointOverflowPolicyTypeDef.ctl"/>
				<Item Name="LVFixedPointQuantizationPolicyTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/fxp/LVFixedPointQuantizationPolicyTypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="LVRGBAColorTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRGBAColorTypeDef.ctl"/>
				<Item Name="NI_3D Picture Control.lvlib" Type="Library" URL="/&lt;vilib&gt;/picture/3D Picture Control/NI_3D Picture Control.lvlib"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_Database_API.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/database/NI_Database_API.lvlib"/>
				<Item Name="NI_Excel.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Excel/NI_Excel.lvclass"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_HTML.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/HTML/NI_HTML.lvclass"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_MABase.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MABase.lvlib"/>
				<Item Name="NI_Matrix.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/Matrix/NI_Matrix.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_PtbyPt.lvlib" Type="Library" URL="/&lt;vilib&gt;/ptbypt/NI_PtbyPt.lvlib"/>
				<Item Name="NI_report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_report.lvclass"/>
				<Item Name="NI_ReportGenerationCore.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_ReportGenerationCore.lvlib"/>
				<Item Name="NI_ReportGenerationToolkit.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_office/NI_ReportGenerationToolkit.lvlib"/>
				<Item Name="NI_Standard Report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Standard Report/NI_Standard Report.lvclass"/>
				<Item Name="ni_tagger_lv_FlushAllConnections.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_FlushAllConnections.vi"/>
				<Item Name="NI_VariableUtilities.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Variable/NI_VariableUtilities.lvlib"/>
				<Item Name="NI_Vision_Acquisition_Software.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/driver/NI_Vision_Acquisition_Software.lvlib"/>
				<Item Name="NI_Vision_Development_Module.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/NI_Vision_Development_Module.lvlib"/>
				<Item Name="NI_XML.lvlib" Type="Library" URL="/&lt;vilib&gt;/xml/NI_XML.lvlib"/>
				<Item Name="NILVSim Get Time and IsAcceptedStep.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Utility/Implementation/NILVSim Get Time and IsAcceptedStep.vi"/>
				<Item Name="NILVSim.dll" Type="Document" URL="/&lt;vilib&gt;/Simulation/Implementation/Shared/NILVSim.dll"/>
				<Item Name="NILVSIM_BeginStep.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Implementation/Shared/NILVSIM_BeginStep.vi"/>
				<Item Name="NILVSim_FinalizeModel.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Implementation/Shared/NILVSim_FinalizeModel.vi"/>
				<Item Name="NILVSim_FinishedLate.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Implementation/Shared/NILVSim_FinishedLate.vi"/>
				<Item Name="NILVSim_GetModelError.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Implementation/Shared/NILVSim_GetModelError.vi"/>
				<Item Name="NILVSim_Initialize.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Implementation/Shared/NILVSim_Initialize.vi"/>
				<Item Name="NILVSim_Manager.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Implementation/Shared/NILVSim_Manager.vi"/>
				<Item Name="NILVSim_ParamChanged.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Implementation/Shared/NILVSim_ParamChanged.vi"/>
				<Item Name="nimc.fb.read.readAnalogLine.axis.vi" Type="VI" URL="/&lt;vilib&gt;/Motion/Express/_s/nimc.fb.read.readAnalogLine.axis.vi"/>
				<Item Name="nimc.fb.read.readDigitalLine.axis.vi" Type="VI" URL="/&lt;vilib&gt;/Motion/Express/_s/nimc.fb.read.readDigitalLine.axis.vi"/>
				<Item Name="nimc.fb.resetPosition.resetPosition.axis.vi" Type="VI" URL="/&lt;vilib&gt;/Motion/Express/_s/nimc.fb.resetPosition.resetPosition.axis.vi"/>
				<Item Name="nimc.fb.stopMove.stop.axis.modeImmediate.vi" Type="VI" URL="/&lt;vilib&gt;/Motion/Express/_s/nimc.fb.stopMove.stop.axis.modeImmediate.vi"/>
				<Item Name="nimc.fb.straightLineMove.startStraightLineMove.axis.modeAbsolute.0.vi" Type="VI" URL="/&lt;vilib&gt;/Motion/Express/_s/nimc.fb.straightLineMove.startStraightLineMove.axis.modeAbsolute.0.vi"/>
				<Item Name="nimc.fb.straightLineMove.startStraightLineMove.axis.modeRelative.0.vi" Type="VI" URL="/&lt;vilib&gt;/Motion/Express/_s/nimc.fb.straightLineMove.startStraightLineMove.axis.modeRelative.0.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="OccFireType.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/OccFireType.ctl"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="Read Characters From File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Characters From File.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Read JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Read JPEG File.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="SIM Bundle Sim Info.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Implementation/Shared/SIM Bundle Sim Info.vi"/>
				<Item Name="SIM derivative (scalar).vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/ContinuousLinear/Implementation/NILVSim Derivative.llb/SIM derivative (scalar).vi"/>
				<Item Name="SIM Fire Discrete SubVI (Core).vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Implementation/Shared/SIM Fire Discrete SubVI (Core).vi"/>
				<Item Name="SIM Fire Discrete SubVI.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Implementation/Shared/SIM Fire Discrete SubVI.vi"/>
				<Item Name="SIM Halt.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Utility/Implementation/SIM Halt.vi"/>
				<Item Name="SIM subVI execution type.ctl" Type="VI" URL="/&lt;vilib&gt;/Simulation/Scripting/Editor Utilities/SIM subVI execution type.ctl"/>
				<Item Name="SIM subVI tag data.ctl" Type="VI" URL="/&lt;vilib&gt;/Simulation/Scripting/Editor Utilities/SIM subVI tag data.ctl"/>
				<Item Name="SIM Time Waveform (vector).vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/SignalDisplay/Implementation/NILVSim Time Waveform.llb/SIM Time Waveform (vector).vi"/>
				<Item Name="SIM Transfer Function (SISO).vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/ContinuousLinear/Implementation/NILVSim Transfer Function.llb/SIM Transfer Function (SISO).vi"/>
				<Item Name="SIM Transfer Function Collector (SISO).vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/ContinuousLinear/Implementation/NILVSim Transfer Function.llb/SIM Transfer Function Collector (SISO).vi"/>
				<Item Name="SIM Transfer Function Distributor (SISO).vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/ContinuousLinear/Implementation/NILVSim Transfer Function.llb/SIM Transfer Function Distributor (SISO).vi"/>
				<Item Name="SIM Unbundle Sim Info.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Implementation/Shared/SIM Unbundle Sim Info.vi"/>
				<Item Name="SimParams.ctl" Type="VI" URL="/&lt;vilib&gt;/Simulation/Scripting/Companion Diagram/SimConfigNode/SimParams.ctl"/>
				<Item Name="SIMPH Block Path to Cluster Path.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Param Hierarchy/PathSupport/SIMPH Block Path to Cluster Path.vi"/>
				<Item Name="SIMPH Block.ctl" Type="VI" URL="/&lt;vilib&gt;/Simulation/Param Hierarchy/Typedefs/SIMPH Block.ctl"/>
				<Item Name="SIMPH Get Root Simulation Info.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Param Hierarchy/HierarchySupport/SIMPH Get Root Simulation Info.vi"/>
				<Item Name="SIMPH Global Control Actions.ctl" Type="VI" URL="/&lt;vilib&gt;/Simulation/Param Hierarchy/Typedefs/SIMPH Global Control Actions.ctl"/>
				<Item Name="SIMPH Param Control Actions.ctl" Type="VI" URL="/&lt;vilib&gt;/Simulation/Param Hierarchy/Typedefs/SIMPH Param Control Actions.ctl"/>
				<Item Name="SIMPH Param Source.ctl" Type="VI" URL="/&lt;vilib&gt;/Simulation/Param Hierarchy/Typedefs/SIMPH Param Source.ctl"/>
				<Item Name="SIMPH Read Cluster Element Value.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Param Hierarchy/SIMPH Read Cluster Element Value.vi"/>
				<Item Name="SIMPH Root Simulation Header.ctl" Type="VI" URL="/&lt;vilib&gt;/Simulation/Param Hierarchy/HierarchySupport/SIMPH Root Simulation Header.ctl"/>
				<Item Name="SIMPH Root Simulation Hierarchy.ctl" Type="VI" URL="/&lt;vilib&gt;/Simulation/Param Hierarchy/HierarchySupport/SIMPH Root Simulation Hierarchy.ctl"/>
				<Item Name="SIMPH Root Simulation Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Simulation/Param Hierarchy/HierarchySupport/SIMPH Root Simulation Type.ctl"/>
				<Item Name="SIMPH System.ctl" Type="VI" URL="/&lt;vilib&gt;/Simulation/Param Hierarchy/Typedefs/SIMPH System.ctl"/>
				<Item Name="SIMPH Write Cluster Element Value.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Param Hierarchy/SIMPH Write Cluster Element Value.vi"/>
				<Item Name="SIMPH_Dot_Path_To_Path_Array.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Param Hierarchy/PathSupport/SIMPH_Dot_Path_To_Path_Array.vi"/>
				<Item Name="SIMPH_Path_Array_To_Dot_Path.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Param Hierarchy/PathSupport/SIMPH_Path_Array_To_Dot_Path.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="SIMSH Read Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Simulation/Implementation/Shared/SIMSH Read Queue.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unescape XML.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Unescape XML.vi"/>
				<Item Name="UnescapeChar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/UnescapeChar.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="Version To Dotted String.vi" Type="VI" URL="/&lt;vilib&gt;/_xctls/Version To Dotted String.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="VISA Flush IO Buffer Mask.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Flush IO Buffer Mask.ctl"/>
				<Item Name="Wait On ActiveX Event.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait On ActiveX Event.vi"/>
				<Item Name="Wait types.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait types.ctl"/>
				<Item Name="WDT Append Waveforms CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms CDB.vi"/>
				<Item Name="WDT Append Waveforms CXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms CXT.vi"/>
				<Item Name="WDT Append Waveforms DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms DBL.vi"/>
				<Item Name="WDT Append Waveforms EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms EXT.vi"/>
				<Item Name="WDT Append Waveforms I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I16.vi"/>
				<Item Name="WDT Append Waveforms I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I32.vi"/>
				<Item Name="WDT Append Waveforms I64.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I64.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write BMP Data To Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data To Buffer.vi"/>
				<Item Name="Write BMP Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data.vi"/>
				<Item Name="Write BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP File.vi"/>
				<Item Name="Write JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Write JPEG File.vi"/>
				<Item Name="Write PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Write PNG File.vi"/>
				<Item Name="XControlSupport.lvlib" Type="Library" URL="/&lt;vilib&gt;/_xctls/XControlSupport.lvlib"/>
			</Item>
			<Item Name="8PulseWidthToPercent.vi" Type="VI" URL="../../drivers/Buses/PWM/8PulseWidthToPercent.vi"/>
			<Item Name="8PWMToPulseWidth.vi" Type="VI" URL="../../drivers/Buses/PWM/8PWMToPulseWidth.vi"/>
			<Item Name="ActuatorRpcServer.lvclass" Type="LVClass" URL="../../main/RemoteProcedureCall/Servers/ActuatorRpcServer/ActuatorRpcServer.lvclass"/>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="ADXL345_Configure.vi" Type="VI" URL="../../drivers/Buses/I2C/Devices/ADXL345/ADXL345_Configure.vi"/>
			<Item Name="ADXL345_Scan.vi" Type="VI" URL="../../drivers/Buses/I2C/Devices/ADXL345/ADXL345_Scan.vi"/>
			<Item Name="ADXL345Setup.ctl" Type="VI" URL="../../drivers/Buses/I2C/Devices/ADXL345/ADXL345Setup.ctl"/>
			<Item Name="Alarm.lvclass" Type="LVClass" URL="../../main/Alarm/Alarm/Alarm.lvclass"/>
			<Item Name="Alarm.xctl" Type="XControl" URL="../../main/Alarm/Alarm/XControl/Alarm/Alarm.xctl"/>
			<Item Name="AlarmConfiguration.xctl" Type="XControl" URL="../../main/Alarm/Alarm/XControl/AlarmConfigurator/AlarmConfiguration.xctl"/>
			<Item Name="AlarmEvent.lvclass" Type="LVClass" URL="../../main/Alarm/AlarmEvent/AlarmEvent.lvclass"/>
			<Item Name="AlarmHandler.lvclass" Type="LVClass" URL="../../main/Alarm/AlarmHandler/AlarmHandler.lvclass"/>
			<Item Name="AlarmInitData.ctl" Type="VI" URL="../../main/Alarm/Alarm/XControl/AlarmConfigurator/AlarmInitData.ctl"/>
			<Item Name="AlarmLister.xctl" Type="XControl" URL="../../main/Alarm/Alarm/XControl/AlarmLister/AlarmLister.xctl"/>
			<Item Name="AlarmObjectAttributes.ctl" Type="VI" URL="../../main/Alarm/Alarm/AlarmObjectAttributes.ctl"/>
			<Item Name="AllegroA3941.vi" Type="VI" URL="../../drivers/FPGA/Drivers/AllegroA3941/AllegroA3941.vi"/>
			<Item Name="AnalysisResult.lvclass" Type="LVClass" URL="../../main/Analysis/AnalysisResult/AnalysisResult.lvclass"/>
			<Item Name="BroadcastEventType.ctl" Type="VI" URL="../../main/GUI/SubPanelUtilities/BroadcastEventType.ctl"/>
			<Item Name="BroadCastIncomingEvent.ctl" Type="VI" URL="../../main/GUI/SubPanelUtilities/BroadCastIncomingEvent.ctl"/>
			<Item Name="BufferedDeviceReader.lvclass" Type="LVClass" URL="../../main/IOStreams/BufferedDeviceReader/BufferedDeviceReader.lvclass"/>
			<Item Name="BufferedImuSerialDeviceReader.lvclass" Type="LVClass" URL="../../drivers/DAQ/BufferedImuSerialDeviceReader/BufferedImuSerialDeviceReader.lvclass"/>
			<Item Name="BufferedReader.lvclass" Type="LVClass" URL="../../main/IOStreams/BufferedReader/BufferedReader.lvclass"/>
			<Item Name="BufferedThreadedReader.lvclass" Type="LVClass" URL="../../main/IOStreams/BufferedThreadedReader/BufferedThreadedReader.lvclass"/>
			<Item Name="CalculateKandM.vi" Type="VI" URL="../../main/Utilities/Calculation/CalculateKandM.vi"/>
			<Item Name="Calibrate.lvclass" Type="LVClass" URL="../../main/Calibration/Calibrate/Calibrate.lvclass"/>
			<Item Name="CalibrateToZero.vi" Type="VI" URL="../../main/Utilities/Waveform/CalibrateToZero.vi"/>
			<Item Name="Calibration.xctl" Type="XControl" URL="../../main/Calibration/Calibrate/XControl/Calibration/Calibration.xctl"/>
			<Item Name="CalibrationLister.xctl" Type="XControl" URL="../../main/Calibration/Calibrate/XControl/CalibrationLister/CalibrationLister.xctl"/>
			<Item Name="CameraDeviceReader.lvclass" Type="LVClass" URL="../../drivers/Vision/CameraDeviceReader/CameraDeviceReader.lvclass"/>
			<Item Name="CameraInputDevice.lvclass" Type="LVClass" URL="../../drivers/Vision/CameraInputDevice/CameraInputDevice.lvclass"/>
			<Item Name="CameraInputStream.lvclass" Type="LVClass" URL="../../drivers/Vision/CameraInputStream/CameraInputStream.lvclass"/>
			<Item Name="CAT24C256_Read.vi" Type="VI" URL="../../drivers/Buses/I2C/Devices/CAT24C256 EEPROM/CAT24C256_Read.vi"/>
			<Item Name="ChannelIdDelimiter.ctl" Type="VI" URL="../../main/Sequence/SequenceInputRunner/ChannelIdDelimiter.ctl"/>
			<Item Name="Codec.lvclass" Type="LVClass" URL="../../main/Codecs/Codec/Codec.lvclass"/>
			<Item Name="Controller.lvclass" Type="LVClass" URL="../../main/ControlSystem/Controller/Controller.lvclass"/>
			<Item Name="CoordinateResult.lvclass" Type="LVClass" URL="../../drivers/Vision/CoordinateResult/CoordinateResult.lvclass"/>
			<Item Name="CRioBufferedDeviceReader.lvclass" Type="LVClass" URL="../../drivers/DAQ/CRioBufferedDeviceReader/CRioBufferedDeviceReader.lvclass"/>
			<Item Name="CRioCSeriesDeviceReader.lvclass" Type="LVClass" URL="../../drivers/DAQ/CRioCSeriesDeviceReader/CRioCSeriesDeviceReader.lvclass"/>
			<Item Name="CRioCSeriesDeviceWriter.lvclass" Type="LVClass" URL="../../drivers/DAQ/CRioCSeriesDeviceWriter/CRioCSeriesDeviceWriter.lvclass"/>
			<Item Name="CRioCSeriesPwmDeviceWriter.lvclass" Type="LVClass" URL="../../drivers/DAQ/CRioCSeriesPwmDeviceWriter/CRioCSeriesPwmDeviceWriter.lvclass"/>
			<Item Name="DataAnalyser.lvclass" Type="LVClass" URL="../../main/Analysis/DataAnalyser/DataAnalyser.lvclass"/>
			<Item Name="Database.lvclass" Type="LVClass" URL="../../main/Storage/Database/Database.lvclass"/>
			<Item Name="DataPattern.lvclass" Type="LVClass" URL="../../main/Analysis/DataPattern/DataPattern.lvclass"/>
			<Item Name="DeviceReader.lvclass" Type="LVClass" URL="../../main/IOStreams/DeviceReader/DeviceReader.lvclass"/>
			<Item Name="DeviceWriter.lvclass" Type="LVClass" URL="../../main/IOStreams/DeviceWriter/DeviceWriter.lvclass"/>
			<Item Name="DisplayLogWriter.lvclass" Type="LVClass" URL="../../main/Logging/DisplayLogWriter/DisplayLogWriter.lvclass"/>
			<Item Name="DOMUserDefRef.dll" Type="Document" URL="DOMUserDefRef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="EchoRpcServer.lvclass" Type="LVClass" URL="../../main/RemoteProcedureCall/Servers/EchoRpcServer/EchoRpcServer.lvclass"/>
			<Item Name="Encode16ChPwm.vi" Type="VI" URL="../../drivers/Buses/PWM/Encode16ChPwm.vi"/>
			<Item Name="Event.lvclass" Type="LVClass" URL="../../main/Events/Event/Event.lvclass"/>
			<Item Name="EventHandler.lvclass" Type="LVClass" URL="../../main/Events/EventHandler/EventHandler.lvclass"/>
			<Item Name="ExecutionBase.lvclass" Type="LVClass" URL="../../main/Execution/ExecutionBase/ExecutionBase.lvclass"/>
			<Item Name="FeedbackController.lvclass" Type="LVClass" URL="../../main/ControlSystem/FeedbackController/FeedbackController.lvclass"/>
			<Item Name="FeedbackControlSequence.lvclass" Type="LVClass" URL="../../main/Sequence/Concrete/FeedbackControlSequence/FeedbackControlSequence.lvclass"/>
			<Item Name="FifoBufferedOutput.vi" Type="VI" URL="../../drivers/DAQ/CRioBufferedDeviceReader/FPGA/FifoBufferedOutput.vi"/>
			<Item Name="FileLogWriter.lvclass" Type="LVClass" URL="../../main/Logging/FileLogWriter/FileLogWriter.lvclass"/>
			<Item Name="FileReader.lvclass" Type="LVClass" URL="../../main/IOStreams/FileReader/FileReader.lvclass"/>
			<Item Name="FileStream.lvclass" Type="LVClass" URL="../../main/IOStreams/FileStream/FileStream.lvclass"/>
			<Item Name="FileWriter.lvclass" Type="LVClass" URL="../../main/IOStreams/FileWriter/FileWriter.lvclass"/>
			<Item Name="FloatingPointSequence.lvclass" Type="LVClass" URL="../../main/Sequence/FloatingPointSequence/FloatingPointSequence.lvclass"/>
			<Item Name="FPGA FIFO_Output.ctl" Type="VI" URL="../../drivers/DAQ/CRioBufferedDeviceReader/FPGA/FPGA FIFO_Output.ctl"/>
			<Item Name="FPGA GPDIO Dummy Engine.vi" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Code/FPGA GPDIO Dummy Engine.vi"/>
			<Item Name="FPGA GPDIO FIFO_Cmd.ctl" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Controls/FPGA GPDIO FIFO_Cmd.ctl"/>
			<Item Name="FPGA GPDIO FIFO_CommandControl Dynamic.ctl" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Controls/FPGA GPDIO FIFO_CommandControl Dynamic.ctl"/>
			<Item Name="FPGA GPDIO FIFO_ReadDataControl.ctl" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Controls/FPGA GPDIO FIFO_ReadDataControl.ctl"/>
			<Item Name="FPGA GPDIO FIFO_WriteDataControl.ctl" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Controls/FPGA GPDIO FIFO_WriteDataControl.ctl"/>
			<Item Name="FPGA GPDIO_16 Port Dynamic RW.vi" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Code/FPGA GPDIO_16 Port Dynamic RW.vi"/>
			<Item Name="FPGA GPDIO_16 Port Dynamic.vi" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Code/FPGA GPDIO_16 Port Dynamic.vi"/>
			<Item Name="FPGA GPDIO_Configuration.ctl" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Controls/FPGA GPDIO_Configuration.ctl"/>
			<Item Name="FPGA GPDIO_Loop State.ctl" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Controls/FPGA GPDIO_Loop State.ctl"/>
			<Item Name="FPGA GPDIO_Read Byte Dynamic.vi" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Code/FPGA GPDIO_Read Byte Dynamic.vi"/>
			<Item Name="FPGA GPDIO_Read Byte State.ctl" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Controls/FPGA GPDIO_Read Byte State.ctl"/>
			<Item Name="FPGA GPDIO_StateMachineData.ctl" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Controls/FPGA GPDIO_StateMachineData.ctl"/>
			<Item Name="FPGA GPDIO_Write Byte Dynamic.vi" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Code/FPGA GPDIO_Write Byte Dynamic.vi"/>
			<Item Name="FPGA GPDIO_Write Byte State.ctl" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Controls/FPGA GPDIO_Write Byte State.ctl"/>
			<Item Name="FPGA I2C Dummy Engine.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/FPGA I2C Dummy Engine.vi"/>
			<Item Name="FPGA I2C EngineControl.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C EngineControl.ctl"/>
			<Item Name="FPGA I2C EngineTimingConfiguration.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C EngineTimingConfiguration.ctl"/>
			<Item Name="FPGA I2C FIFO_Cmd Dynamic.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C FIFO_Cmd Dynamic.ctl"/>
			<Item Name="FPGA I2C FIFO_Cmd.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C FIFO_Cmd.ctl"/>
			<Item Name="FPGA I2C FIFO_CommandControl Dynamic.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C FIFO_CommandControl Dynamic.ctl"/>
			<Item Name="FPGA I2C FIFO_CommandControl.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C FIFO_CommandControl.ctl"/>
			<Item Name="FPGA I2C FIFO_ReadDataControl.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C FIFO_ReadDataControl.ctl"/>
			<Item Name="FPGA I2C FIFO_WriteDataControl.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C FIFO_WriteDataControl.ctl"/>
			<Item Name="FPGA I2C StateMachineData.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C StateMachineData.ctl"/>
			<Item Name="FPGA I2C_Cmd.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C_Cmd.ctl"/>
			<Item Name="FPGA I2C_Comm Loop State.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C_Comm Loop State.ctl"/>
			<Item Name="FPGA I2C_I2C 5 Port Dynamic RW.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/FPGA I2C_I2C 5 Port Dynamic RW.vi"/>
			<Item Name="FPGA I2C_I2C 5 Port Dynamic.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/FPGA I2C_I2C 5 Port Dynamic.vi"/>
			<Item Name="FPGA I2C_I2C Loop State.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C_I2C Loop State.ctl"/>
			<Item Name="FPGA I2C_I2C Port2 RW.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/FPGA I2C_I2C Port2 RW.vi"/>
			<Item Name="FPGA I2C_I2C Port2.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/FPGA I2C_I2C Port2.vi"/>
			<Item Name="FPGA I2C_Read Byte Dynamic.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/FPGA I2C_Read Byte Dynamic.vi"/>
			<Item Name="FPGA I2C_Read Byte State.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C_Read Byte State.ctl"/>
			<Item Name="FPGA I2C_Read Byte.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/FPGA I2C_Read Byte.vi"/>
			<Item Name="FPGA I2C_Start Dynamic.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/FPGA I2C_Start Dynamic.vi"/>
			<Item Name="FPGA I2C_Start State.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C_Start State.ctl"/>
			<Item Name="FPGA I2C_Start.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/FPGA I2C_Start.vi"/>
			<Item Name="FPGA I2C_Stop Dynamic.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/FPGA I2C_Stop Dynamic.vi"/>
			<Item Name="FPGA I2C_Stop State.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C_Stop State.ctl"/>
			<Item Name="FPGA I2C_Stop.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/FPGA I2C_Stop.vi"/>
			<Item Name="FPGA I2C_Write Byte Dynamic.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/FPGA I2C_Write Byte Dynamic.vi"/>
			<Item Name="FPGA I2C_Write Byte State.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA I2C_Write Byte State.ctl"/>
			<Item Name="FPGA I2C_Write Byte.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/FPGA I2C_Write Byte.vi"/>
			<Item Name="FPGA Memory_CalibrationControl.ctl" Type="VI" URL="../../drivers/DAQ/CRioBufferedDeviceReader/FPGA/FPGA Memory_CalibrationControl.ctl"/>
			<Item Name="FPGA Memory_MeasurementsControl.ctl" Type="VI" URL="../../drivers/DAQ/CRioBufferedDeviceReader/FPGA/FPGA Memory_MeasurementsControl.ctl"/>
			<Item Name="FPGA SPI Dummy Engine.vi" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Code/FPGA SPI Dummy Engine.vi"/>
			<Item Name="FPGA SPI Dummy EngineIO.vi" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Code/FPGA SPI Dummy EngineIO.vi"/>
			<Item Name="FPGA SPI FIFO_Cmd.ctl" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Controls/FPGA SPI FIFO_Cmd.ctl"/>
			<Item Name="FPGA SPI FIFO_ReadDataControl.ctl" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Controls/FPGA SPI FIFO_ReadDataControl.ctl"/>
			<Item Name="FPGA SPI FIFO_WriteDataControl.ctl" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Controls/FPGA SPI FIFO_WriteDataControl.ctl"/>
			<Item Name="FPGA SPI_Cmd.ctl" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Controls/FPGA SPI_Cmd.ctl"/>
			<Item Name="FPGA SPI_FIFO_CommandControl.ctl" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Controls/FPGA SPI_FIFO_CommandControl.ctl"/>
			<Item Name="FPGA SPI_SPI Loop State Enum.ctl" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Controls/FPGA SPI_SPI Loop State Enum.ctl"/>
			<Item Name="FPGA SPI_SPI Port.vi" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Code/FPGA SPI_SPI Port.vi"/>
			<Item Name="FPGA SPI_SPI RW.vi" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Code/FPGA SPI_SPI RW.vi"/>
			<Item Name="FPGA SPI_SPI State Data.ctl" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Controls/FPGA SPI_SPI State Data.ctl"/>
			<Item Name="FPGA SPI_SPI StateMachine Data.ctl" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Controls/FPGA SPI_SPI StateMachine Data.ctl"/>
			<Item Name="FPGA UART 5 Port Dynamic V2 RW.vi" Type="VI" URL="../../drivers/Buses/UART/FPGA/Code/FPGA UART 5 Port Dynamic V2 RW.vi"/>
			<Item Name="FPGA UART 5 Port Dynamic V2.vi" Type="VI" URL="../../drivers/Buses/UART/FPGA/Code/FPGA UART 5 Port Dynamic V2.vi"/>
			<Item Name="FPGA UART Config.ctl" Type="VI" URL="../../drivers/Buses/UART/FPGA/Controls/FPGA UART Config.ctl"/>
			<Item Name="FPGA UART Dummy Engine.vi" Type="VI" URL="../../drivers/Buses/UART/FPGA/Code/FPGA UART Dummy Engine.vi"/>
			<Item Name="FPGA UART Engine_Cmd.ctl" Type="VI" URL="../../drivers/Buses/UART/FPGA/Controls/FPGA UART Engine_Cmd.ctl"/>
			<Item Name="FPGA UART Engine_LoopState.ctl" Type="VI" URL="../../drivers/Buses/UART/FPGA/Controls/FPGA UART Engine_LoopState.ctl"/>
			<Item Name="FPGA UART FIFO_Cmd.ctl" Type="VI" URL="../../drivers/Buses/UART/FPGA/Controls/FPGA UART FIFO_Cmd.ctl"/>
			<Item Name="FPGA UART FIFO_CommandControl.ctl" Type="VI" URL="../../drivers/Buses/UART/FPGA/Controls/FPGA UART FIFO_CommandControl.ctl"/>
			<Item Name="FPGA UART FIFO_ReadDataControl.ctl" Type="VI" URL="../../drivers/Buses/UART/FPGA/Controls/FPGA UART FIFO_ReadDataControl.ctl"/>
			<Item Name="FPGA UART FIFO_WriteDataControl.ctl" Type="VI" URL="../../drivers/Buses/UART/FPGA/Controls/FPGA UART FIFO_WriteDataControl.ctl"/>
			<Item Name="FPGA UART IO Cluster Dynamic.ctl" Type="VI" URL="../../drivers/Buses/UART/FPGA/Controls/FPGA UART IO Cluster Dynamic.ctl"/>
			<Item Name="FPGA UART IO Cluster.ctl" Type="VI" URL="../../drivers/Buses/UART/FPGA/Controls/FPGA UART IO Cluster.ctl"/>
			<Item Name="FPGA UART StateReadData.ctl" Type="VI" URL="../../drivers/Buses/UART/FPGA/Controls/FPGA UART StateReadData.ctl"/>
			<Item Name="FPGA UART StateWriteData.ctl" Type="VI" URL="../../drivers/Buses/UART/FPGA/Controls/FPGA UART StateWriteData.ctl"/>
			<Item Name="FPGA_GPDIO IO Cluster Dynamic.ctl" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Controls/FPGA_GPDIO IO Cluster Dynamic.ctl"/>
			<Item Name="FPGA_GPDIO IO Cluster.ctl" Type="VI" URL="../../drivers/Buses/GPDIO/FPGA/Controls/FPGA_GPDIO IO Cluster.ctl"/>
			<Item Name="FPGA_I2C IO Cluster Dynamic.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA_I2C IO Cluster Dynamic.ctl"/>
			<Item Name="FPGA_I2C IO Cluster.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA_I2C IO Cluster.ctl"/>
			<Item Name="FPGA_I2C Speed.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/FPGA_I2C Speed.ctl"/>
			<Item Name="fpga_main.lvbitx" Type="Document" URL="../../swepart/transmonitor/Builds/FPGA/fpga_main.lvbitx"/>
			<Item Name="FPGA_SPI IO Cluster.ctl" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Controls/FPGA_SPI IO Cluster.ctl"/>
			<Item Name="FunctionCalibration.lvclass" Type="LVClass" URL="../../main/Calibration/FunctionCalibration/FunctionCalibration.lvclass"/>
			<Item Name="Generate Digital Pulse (uSec).vi" Type="VI" URL="../../drivers/FPGA/SignalGeneration/Generate Digital Pulse (uSec).vi"/>
			<Item Name="GetCalibration.vi" Type="VI" URL="../../main/IOStreams/DeviceReader/GetCalibration.vi"/>
			<Item Name="GetConcreteClasses.vi" Type="VI" URL="../../main/GUI/SubPanelUtilities/GetConcreteClasses.vi"/>
			<Item Name="GetRawSocketFromConnectionID.vi" Type="VI" URL="../../main/IOStreams/SupportVIs/GetRawSocketFromConnectionID.vi"/>
			<Item Name="HIH6100_Scan.vi" Type="VI" URL="../../drivers/Buses/I2C/Devices/HIH6100/HIH6100_Scan.vi"/>
			<Item Name="HMC5583_Configure.vi" Type="VI" URL="../../drivers/Buses/I2C/Devices/HMC5583/HMC5583_Configure.vi"/>
			<Item Name="HMC5583_Scan.vi" Type="VI" URL="../../drivers/Buses/I2C/Devices/HMC5583/HMC5583_Scan.vi"/>
			<Item Name="HMC5583LSetup.ctl" Type="VI" URL="../../drivers/Buses/I2C/Devices/HMC5583/HMC5583LSetup.ctl"/>
			<Item Name="HmsProfibusSimulator.lvlib" Type="Library" URL="../../drivers/Buses/Profibus/HmsProfibusSimulator/HmsProfibusSimulator.lvlib"/>
			<Item Name="HmsSerialToProfibusDeviceReader.lvclass" Type="LVClass" URL="../../drivers/DAQ/HmsSerialToProfibusDeviceReader/HmsSerialToProfibusDeviceReader.lvclass"/>
			<Item Name="HmsSerialToProfibusDeviceWriter.lvclass" Type="LVClass" URL="../../drivers/DAQ/HmsSerialToProfibusDeviceWriter/HmsSerialToProfibusDeviceWriter.lvclass"/>
			<Item Name="Host to FPGA DispatchControl.ctl" Type="VI" URL="../../drivers/Buses/Dispatch/Host to FPGA DispatchControl.ctl"/>
			<Item Name="HSCMRRN001PD2A3_Scan.vi" Type="VI" URL="../../drivers/Buses/I2C/Devices/HSCMRRN001PD2A3/HSCMRRN001PD2A3_Scan.vi"/>
			<Item Name="HtmlFileLogWriter.lvclass" Type="LVClass" URL="../../main/Logging/HtmlFileLogWriter/HtmlFileLogWriter.lvclass"/>
			<Item Name="I2C Configuration Parameters.ctl" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Controls/I2C Configuration Parameters.ctl"/>
			<Item Name="I2CToMode.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/I2CToMode.vi"/>
			<Item Name="ImageFeature.lvclass" Type="LVClass" URL="../../drivers/Vision/ImageFeature/ImageFeature.lvclass"/>
			<Item Name="ImageProcessor.lvclass" Type="LVClass" URL="../../drivers/Vision/ImageProcessor/ImageProcessor.lvclass"/>
			<Item Name="IMAQdxCInputDevice.lvclass" Type="LVClass" URL="../../drivers/Vision/IMAQdxCIDevice/IMAQdxCInputDevice.lvclass"/>
			<Item Name="IncomingUserEvent.ctl" Type="VI" URL="../../main/GUI/SubPanelUtilities/IncomingUserEvent.ctl"/>
			<Item Name="IndexedThreadedWriter.lvclass" Type="LVClass" URL="../../main/IOStreams/IndexedThreadedWriter/IndexedThreadedWriter.lvclass"/>
			<Item Name="InitData.ctl" Type="VI" URL="../../main/ControlSystem/Regulator/XControl/RegulatorConfiguration/InitData.ctl"/>
			<Item Name="InputDispatcher_GPDIO.vi" Type="VI" URL="../RIOimu/FPGA/InputDispatcher_GPDIO.vi"/>
			<Item Name="InputStream.lvlib" Type="Library" URL="../../main/IOStreams/InputStream/InputStream.lvlib"/>
			<Item Name="IO.lvclass" Type="LVClass" URL="../../main/IOStreams/IO/IO.lvclass"/>
			<Item Name="IOCluster.ctl" Type="VI" URL="../../drivers/FPGA/Drivers/AllegroA3941/IOCluster.ctl"/>
			<Item Name="ITG3200_Configure.vi" Type="VI" URL="../../drivers/Buses/I2C/Devices/ITG3200/ITG3200_Configure.vi"/>
			<Item Name="ITG3200_Scan.vi" Type="VI" URL="../../drivers/Buses/I2C/Devices/ITG3200/ITG3200_Scan.vi"/>
			<Item Name="ITG3200Setup.ctl" Type="VI" URL="../../drivers/Buses/I2C/Devices/ITG3200/ITG3200Setup.ctl"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="LeadLag.vi" Type="VI" URL="../../main/ControlSystem/Concrete/PIDRegulator/LeadLag.vi"/>
			<Item Name="LinearCalibration.lvclass" Type="LVClass" URL="../../main/Calibration/Concrete/LinearCalibration/LinearCalibration.lvclass"/>
			<Item Name="ListFilesWithFileTypes.vi" Type="VI" URL="../../main/Utilities/Path/ListFilesWithFileTypes.vi"/>
			<Item Name="ListLogWriter.lvclass" Type="LVClass" URL="../../main/Logging/ListLogWriter/ListLogWriter.lvclass"/>
			<Item Name="Logger.lvclass" Type="LVClass" URL="../../main/Logging/Logger/Logger.lvclass"/>
			<Item Name="LogWriter.lvclass" Type="LVClass" URL="../../main/Logging/LogWriter/LogWriter.lvclass"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="MadgwickOrientationFilter.lvlib" Type="Library" URL="../../main/Analysis/Algorithm/MadgwickAhrs/MadgwickOrientationFilter.lvlib"/>
			<Item Name="Main.lvbitx" Type="Document" URL="../../oceanharvesting/prototype/target/Binaries/fpga/Bitfile/Main.lvbitx"/>
			<Item Name="MemoryReader.lvclass" Type="LVClass" URL="../../main/IOStreams/MemoryReader/MemoryReader.lvclass"/>
			<Item Name="MemoryStream.lvclass" Type="LVClass" URL="../../main/IOStreams/MemoryStream/MemoryStream.lvclass"/>
			<Item Name="MemoryWriter.lvclass" Type="LVClass" URL="../../main/IOStreams/MemoryWriter/MemoryWriter.lvclass"/>
			<Item Name="MinMaxAlarm.lvclass" Type="LVClass" URL="../../main/Alarm/Concrete/MinMaxAlarm/MinMaxAlarm.lvclass"/>
			<Item Name="MinMaxAlarm.xctl" Type="XControl" URL="../../main/Alarm/Concrete/MinMaxAlarm/XControl/MinMaxAlarm/MinMaxAlarm.xctl"/>
			<Item Name="MinMaxObjectAttributes.ctl" Type="VI" URL="../../main/Alarm/Concrete/MinMaxAlarm/MinMaxObjectAttributes.ctl"/>
			<Item Name="ModeToI2CParameters.vi" Type="VI" URL="../../drivers/Buses/I2C/FPGA/Code/ModeToI2CParameters.vi"/>
			<Item Name="ModeToSPIParameters.vi" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Code/ModeToSPIParameters.vi"/>
			<Item Name="ModeToUARTParameters.vi" Type="VI" URL="../../drivers/Buses/UART/FPGA/Code/ModeToUARTParameters.vi"/>
			<Item Name="MPU9250_AccelAndGyro.vi" Type="VI" URL="../../drivers/Buses/SPI/Devices/MPU9250/MPU9250_AccelAndGyro.vi"/>
			<Item Name="MPU9250_AccelAndGyro_Continous.vi" Type="VI" URL="../../drivers/Buses/SPI/Devices/MPU9250/MPU9250_AccelAndGyro_Continous.vi"/>
			<Item Name="MPU9250_Magneto.vi" Type="VI" URL="../../drivers/Buses/SPI/Devices/MPU9250/MPU9250_Magneto.vi"/>
			<Item Name="MPU9250_Scan.vi" Type="VI" URL="../../drivers/Buses/SPI/Devices/MPU9250/MPU9250_Scan.vi"/>
			<Item Name="nidcpower_32.dll" Type="Document" URL="nidcpower_32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="niimaqdx.dll" Type="Document" URL="niimaqdx.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nilvaiu.dll" Type="Document" URL="nilvaiu.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nivision.dll" Type="Document" URL="nivision.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nivissvc.dll" Type="Document" URL="nivissvc.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="OutgoingUserEvent.ctl" Type="VI" URL="../../main/GUI/SubPanelUtilities/OutgoingUserEvent.ctl"/>
			<Item Name="OutgoingUserEventArray.ctl" Type="VI" URL="../../main/GUI/SubPanelUtilities/OutgoingUserEventArray.ctl"/>
			<Item Name="OutputDispatcher_GPDIO.vi" Type="VI" URL="../RIOimu/FPGA/OutputDispatcher_GPDIO.vi"/>
			<Item Name="OutputStream.lvclass" Type="LVClass" URL="../../main/IOStreams/OutputStream/OutputStream.lvclass"/>
			<Item Name="Percent2PulseWidth.vi" Type="VI" URL="../../drivers/Buses/PWM/Percent2PulseWidth.vi"/>
			<Item Name="Persist.lvclass" Type="LVClass" URL="../../main/Utilities/Persist/Persist.lvclass"/>
			<Item Name="PhaseControlEnum.ctl" Type="VI" URL="../../drivers/FPGA/Drivers/AllegroA3941/PhaseControlEnum.ctl"/>
			<Item Name="PIDRegulator.lvclass" Type="LVClass" URL="../../main/ControlSystem/Concrete/PIDRegulator/PIDRegulator.lvclass"/>
			<Item Name="PidRegulator.xctl" Type="XControl" URL="../../main/ControlSystem/Concrete/PIDRegulator/XControl/PidRegulator.xctl"/>
			<Item Name="PidRegulatorObjectAttributes.ctl" Type="VI" URL="../../main/ControlSystem/Concrete/PIDRegulator/PidRegulatorObjectAttributes.ctl"/>
			<Item Name="PWMDriver.vi" Type="VI" URL="../../drivers/FPGA/Drivers/AllegroA3941/PWMDriver.vi"/>
			<Item Name="Regulator.lvclass" Type="LVClass" URL="../../main/ControlSystem/Regulator/Regulator.lvclass"/>
			<Item Name="Regulator.xctl" Type="XControl" URL="../../main/ControlSystem/Regulator/XControl/Regulator/Regulator.xctl"/>
			<Item Name="RegulatorConfiguration.xctl" Type="XControl" URL="../../main/ControlSystem/Regulator/XControl/RegulatorConfiguration/RegulatorConfiguration.xctl"/>
			<Item Name="RegulatorLister.xctl" Type="XControl" URL="../../main/ControlSystem/Regulator/XControl/RegulatorLister/RegulatorLister.xctl"/>
			<Item Name="RegulatorObjectAttributes.ctl" Type="VI" URL="../../main/ControlSystem/Regulator/RegulatorObjectAttributes.ctl"/>
			<Item Name="ReplaceCalibration.vi" Type="VI" URL="../../main/IOStreams/DeviceReader/ReplaceCalibration.vi"/>
			<Item Name="RingBuffer.lvlib" Type="Library" URL="../../main/Buffer/RingBuffer/RingBuffer.lvlib"/>
			<Item Name="RoundImageFeature.lvclass" Type="LVClass" URL="../../drivers/Vision/RoundImageFeature/RoundImageFeature.lvclass"/>
			<Item Name="RpcClient.lvclass" Type="LVClass" URL="../../main/RemoteProcedureCall/RpcClient/RpcClient.lvclass"/>
			<Item Name="RpcServer.lvclass" Type="LVClass" URL="../../main/RemoteProcedureCall/RpcServer/RpcServer.lvclass"/>
			<Item Name="Scale.vi" Type="VI" URL="../../main/Utilities/Waveform/Scale.vi"/>
			<Item Name="Sequence.xctl" Type="XControl" URL="../../main/Sequence/SequenceInputRunner/XControl/Sequence.xctl"/>
			<Item Name="SequenceHandler.lvclass" Type="LVClass" URL="../../main/Sequence/SequenceHandler/SequenceHandler.lvclass"/>
			<Item Name="SequenceInputRunner.lvclass" Type="LVClass" URL="../../main/Sequence/SequenceInputRunner/SequenceInputRunner.lvclass"/>
			<Item Name="SequenceInputRunnerObjectAttributes.ctl" Type="VI" URL="../../main/Sequence/SequenceInputRunner/SequenceInputRunnerObjectAttributes.ctl"/>
			<Item Name="SequenceLister.xctl" Type="XControl" URL="../../main/Sequence/SequenceHandler/SequenceLister/SequenceLister.xctl"/>
			<Item Name="SequenceSeriesType.ctl" Type="VI" URL="../../main/Sequence/SequenceInputRunner/SequenceSeriesType.ctl"/>
			<Item Name="SetWaveformChannelNames.vi" Type="VI" URL="../../main/Utilities/Waveform/SetWaveformChannelNames.vi"/>
			<Item Name="SimulatedBufferedDeviceReader.lvclass" Type="LVClass" URL="../../drivers/DAQ/SimulatedBufferedDeviceReader/SimulatedBufferedDeviceReader.lvclass"/>
			<Item Name="SimulatedFileCameraInputDevice.lvclass" Type="LVClass" URL="../../drivers/Vision/SimulatedFileCameraInputDevice/SimulatedFileCameraInputDevice.lvclass"/>
			<Item Name="SimulatedUnbufferedDeviceWriter.lvclass" Type="LVClass" URL="../../drivers/DAQ/SimulatedUnbufferedDeviceWriter/SimulatedUnbufferedDeviceWriter.lvclass"/>
			<Item Name="SmuDeviceReader.lvclass" Type="LVClass" URL="../../drivers/DAQ/SmuDeviceReader/SmuDeviceReader.lvclass"/>
			<Item Name="SmuDeviceWriter.lvclass" Type="LVClass" URL="../../drivers/DAQ/SmuDeviceWriter/SmuDeviceWriter.lvclass"/>
			<Item Name="SocketReader.lvclass" Type="LVClass" URL="../../main/IOStreams/SocketReader/SocketReader.lvclass"/>
			<Item Name="SocketStream.lvclass" Type="LVClass" URL="../../main/IOStreams/SocketStream/SocketStream.lvclass"/>
			<Item Name="SocketWriter.lvclass" Type="LVClass" URL="../../main/IOStreams/SocketWriter/SocketWriter.lvclass"/>
			<Item Name="SPI Configuration Parameters.ctl" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Controls/SPI Configuration Parameters.ctl"/>
			<Item Name="SPI_States.ctl" Type="VI" URL="../../drivers/Buses/SPI/Devices/MPU9250/Controls/SPI_States.ctl"/>
			<Item Name="SPIOutputEnable.vi" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Code/SPIOutputEnable.vi"/>
			<Item Name="SPIToMode.vi" Type="VI" URL="../../drivers/Buses/SPI/FPGA/Code/SPIToMode.vi"/>
			<Item Name="SSCMRNN015PA3A3_Scan.vi" Type="VI" URL="../../drivers/Buses/I2C/Devices/SSCMRNN015PA3A3/SSCMRNN015PA3A3_Scan.vi"/>
			<Item Name="State.lvclass" Type="LVClass" URL="../../main/StateMachine/States/State/State.lvclass"/>
			<Item Name="StateMachine.lvclass" Type="LVClass" URL="../../main/StateMachine/StateMachine/StateMachine.lvclass"/>
			<Item Name="StateMachineEvent.lvclass" Type="LVClass" URL="../../main/StateMachine/StateMachineEvent/StateMachineEvent.lvclass"/>
			<Item Name="Storage.lvclass" Type="LVClass" URL="../../main/Storage/Storage/Storage.lvclass"/>
			<Item Name="Stream.lvclass" Type="LVClass" URL="../../main/IOStreams/Stream/Stream.lvclass"/>
			<Item Name="SubPanelIncomingData.ctl" Type="VI" URL="../../main/GUI/SubPanelUtilities/SubPanelIncomingData.ctl"/>
			<Item Name="SubPanelInitiate.vi" Type="VI" URL="../../main/GUI/SubPanelUtilities/SubPanelInitiate.vi"/>
			<Item Name="SubPanelOutgoingData.ctl" Type="VI" URL="../../main/GUI/SubPanelUtilities/SubPanelOutgoingData.ctl"/>
			<Item Name="SubPanelOutgoingDataArray.ctl" Type="VI" URL="../../main/GUI/SubPanelUtilities/SubPanelOutgoingDataArray.ctl"/>
			<Item Name="Tare.vi" Type="VI" URL="../../main/Utilities/Waveform/Tare.vi"/>
			<Item Name="TaskBufferedDeviceReader.lvclass" Type="LVClass" URL="../../drivers/DAQ/TaskBufferedDeviceReader/TaskBufferedDeviceReader.lvclass"/>
			<Item Name="TaskUnbufferedDeviceWriter.lvclass" Type="LVClass" URL="../../drivers/DAQ/TaskUnbufferedDeviceWriter/TaskUnbufferedDeviceWriter.lvclass"/>
			<Item Name="TCP_NODELAY.vi" Type="VI" URL="../../main/IOStreams/SupportVIs/TCP_NODELAY.vi"/>
			<Item Name="TCPServer.lvclass" Type="LVClass" URL="../../main/Networking/TCPServer/TCPServer.lvclass"/>
			<Item Name="TestApi.lvclass" Type="LVClass" URL="../../main/TestSystem/TestApi/TestApi.lvclass"/>
			<Item Name="TestSequence.lvclass" Type="LVClass" URL="../../main/TestSystem/TestSequence/TestSequence.lvclass"/>
			<Item Name="TestSequenceExecutive.xctl" Type="XControl" URL="../../main/TestSystem/TestSequence/XControls/Executive/TestSequenceExecutive.xctl"/>
			<Item Name="TestSequenceLogWriter.lvclass" Type="LVClass" URL="../../main/Logging/TestSequenceLogWriter/TestSequenceLogWriter.lvclass"/>
			<Item Name="TestSequenceRunner.lvclass" Type="LVClass" URL="../../main/TestSystem/TestSequenceRunner/TestSequenceRunner.lvclass"/>
			<Item Name="ThreadedWriter.lvclass" Type="LVClass" URL="../../main/IOStreams/ThreadedWriter/ThreadedWriter.lvclass"/>
			<Item Name="Threading.lvclass" Type="LVClass" URL="../../main/Execution/Threading/Threading.lvclass"/>
			<Item Name="TimedEnd.lvclass" Type="LVClass" URL="../../main/StateMachine/States/Timed/TimedEnd/TimedEnd.lvclass"/>
			<Item Name="TimedEnter.lvclass" Type="LVClass" URL="../../main/StateMachine/States/Timed/TimedEnter/TimedEnter.lvclass"/>
			<Item Name="TimedExit.lvclass" Type="LVClass" URL="../../main/StateMachine/States/Timed/TimedExit/TimedExit.lvclass"/>
			<Item Name="TimedNominal.lvclass" Type="LVClass" URL="../../main/StateMachine/States/Timed/TimedNominal/TimedNominal.lvclass"/>
			<Item Name="TimedStart.lvclass" Type="LVClass" URL="../../main/StateMachine/States/Timed/TimedStart/TimedStart.lvclass"/>
			<Item Name="TimedState.lvclass" Type="LVClass" URL="../../main/StateMachine/States/Timed/TimedState/TimedState.lvclass"/>
			<Item Name="TimedStateMachine.lvclass" Type="LVClass" URL="../../main/StateMachine/TimedStateMachine/TimedStateMachine.lvclass"/>
			<Item Name="UART Configuration Parameters.ctl" Type="VI" URL="../../drivers/Buses/UART/FPGA/Controls/UART Configuration Parameters.ctl"/>
			<Item Name="UART_Firmware.lvlib" Type="Library" URL="../../drivers/Buses/UART/FPGA/Code/UART_Firmware.lvlib"/>
			<Item Name="UARTOutputEnable.vi" Type="VI" URL="../../drivers/Buses/UART/FPGA/Code/UARTOutputEnable.vi"/>
			<Item Name="UARTToMode.vi" Type="VI" URL="../../drivers/Buses/UART/FPGA/Code/UARTToMode.vi"/>
			<Item Name="UnbufferedDeviceReader.lvclass" Type="LVClass" URL="../../main/IOStreams/UnbufferedDeviceReader/UnbufferedDeviceReader.lvclass"/>
			<Item Name="UnbufferedDeviceWriter.lvclass" Type="LVClass" URL="../../main/IOStreams/UnbufferedDeviceWriter/UnbufferedDeviceWriter.lvclass"/>
			<Item Name="url_decode.vi" Type="VI" URL="../../main/Utilities/String/url_decode.vi"/>
			<Item Name="UserDefinedErrorReasons.lvclass" Type="LVClass" URL="../../main/Logging/UserDefinedErrorReasons/UserDefinedErrorReasons.lvclass"/>
			<Item Name="UserEventType.ctl" Type="VI" URL="../../main/GUI/SubPanelUtilities/UserEventType.ctl"/>
			<Item Name="UUTBase.lvclass" Type="LVClass" URL="../../main/TestSystem/UUTBase/UUTBase.lvclass"/>
			<Item Name="WaitForOpen.vi" Type="VI" URL="../../main/GUI/SubPanelUtilities/WaitForOpen.vi"/>
			<Item Name="wsock32.dll" Type="Document" URL="wsock32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="XML.lvclass" Type="LVClass" URL="../../main/Storage/XML/XML.lvclass"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
