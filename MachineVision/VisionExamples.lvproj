﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="11008008">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Images" Type="Folder" URL="../Implementations/Images">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="DetectRecession.vi" Type="VI" URL="../Implementations/DetectRecession.vi"/>
		<Item Name="CaptureImage.vi" Type="VI" URL="../Implementations/CaptureImage.vi"/>
		<Item Name="IMAQdxCInputDevice.lvclass" Type="LVClass" URL="../../../drivers/Vision/IMAQdxCIDevice/IMAQdxCInputDevice.lvclass"/>
		<Item Name="RoundImageFeature.lvclass" Type="LVClass" URL="../../../drivers/Vision/RoundImageFeature/RoundImageFeature.lvclass"/>
		<Item Name="CoordinateResult.lvclass" Type="LVClass" URL="../../../drivers/Vision/CoordinateResult/CoordinateResult.lvclass"/>
		<Item Name="SimulatedFileCameraInputDevice.lvclass" Type="LVClass" URL="../../../drivers/Vision/SimulatedFileCameraInputDevice/SimulatedFileCameraInputDevice.lvclass"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="IMAQ Clear Overlay" Type="VI" URL="/&lt;vilib&gt;/vision/Overlay.llb/IMAQ Clear Overlay"/>
				<Item Name="IMAQ Image.ctl" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/IMAQ Image.ctl"/>
				<Item Name="IMAQ Overlay Oval" Type="VI" URL="/&lt;vilib&gt;/vision/Overlay.llb/IMAQ Overlay Oval"/>
				<Item Name="IMAQdx.ctl" Type="VI" URL="/&lt;vilib&gt;/userdefined/High Color/IMAQdx.ctl"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="IMAQ Dispose" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Dispose"/>
				<Item Name="IMAQ Create" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ Create"/>
				<Item Name="Image Type" Type="VI" URL="/&lt;vilib&gt;/vision/Image Controls.llb/Image Type"/>
				<Item Name="NI_Vision_Acquisition_Software.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/driver/NI_Vision_Acquisition_Software.lvlib"/>
				<Item Name="IMAQ ArrayToImage" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ ArrayToImage"/>
				<Item Name="NI_Vision_Development_Module.lvlib" Type="Library" URL="/&lt;vilib&gt;/vision/NI_Vision_Development_Module.lvlib"/>
				<Item Name="IMAQ ImageToArray" Type="VI" URL="/&lt;vilib&gt;/vision/Basics.llb/IMAQ ImageToArray"/>
				<Item Name="IMAQ ReadFile" Type="VI" URL="/&lt;vilib&gt;/vision/Files.llb/IMAQ ReadFile"/>
				<Item Name="IMAQ Copy" Type="VI" URL="/&lt;vilib&gt;/vision/Management.llb/IMAQ Copy"/>
				<Item Name="XDNodeRunTimeDep.lvlib" Type="Library" URL="/&lt;vilib&gt;/Platform/TimedLoop/XDataNode/XDNodeRunTimeDep.lvlib"/>
			</Item>
			<Item Name="CameraInputDevice.lvclass" Type="LVClass" URL="../../../drivers/Vision/CameraInputDevice/CameraInputDevice.lvclass"/>
			<Item Name="nivision.dll" Type="Document" URL="nivision.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="CameraInputStream.lvclass" Type="LVClass" URL="../../../drivers/Vision/CameraInputStream/CameraInputStream.lvclass"/>
			<Item Name="ExecutionBase.lvclass" Type="LVClass" URL="../../../main/Execution/ExecutionBase/ExecutionBase.lvclass"/>
			<Item Name="InputStream.lvlib" Type="Library" URL="../../../main/IOStreams/InputStream/InputStream.lvlib"/>
			<Item Name="IO.lvclass" Type="LVClass" URL="../../../main/IOStreams/IO/IO.lvclass"/>
			<Item Name="Calibrate.lvclass" Type="LVClass" URL="../../../main/Calibration/Calibrate/Calibrate.lvclass"/>
			<Item Name="Threading.lvclass" Type="LVClass" URL="../../../main/Execution/Threading/Threading.lvclass"/>
			<Item Name="nivissvc.dll" Type="Document" URL="nivissvc.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="CameraDeviceReader.lvclass" Type="LVClass" URL="../../../drivers/Vision/CameraDeviceReader/CameraDeviceReader.lvclass"/>
			<Item Name="niimaqdx.dll" Type="Document" URL="niimaqdx.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="ImageFeature.lvclass" Type="LVClass" URL="../../../drivers/Vision/ImageFeature/ImageFeature.lvclass"/>
			<Item Name="DataPattern.lvclass" Type="LVClass" URL="../../../main/Analysis/DataPattern/DataPattern.lvclass"/>
			<Item Name="AnalysisResult.lvclass" Type="LVClass" URL="../../../main/Analysis/AnalysisResult/AnalysisResult.lvclass"/>
			<Item Name="ImageProcessor.lvclass" Type="LVClass" URL="../../../drivers/Vision/ImageProcessor/ImageProcessor.lvclass"/>
			<Item Name="DataAnalyser.lvclass" Type="LVClass" URL="../../../main/Analysis/DataAnalyser/DataAnalyser.lvclass"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
