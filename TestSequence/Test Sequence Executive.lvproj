﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="SupportFiles" Type="Folder" URL="../SupportFiles">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Test Sequences" Type="Folder">
			<Item Name="FlashUUTTestSequence.lvclass" Type="LVClass" URL="../FlashUUTTestSequence/FlashUUTTestSequence.lvclass"/>
			<Item Name="ExampleTestSequence.lvclass" Type="LVClass" URL="../ExampleTestSequence/ExampleTestSequence.lvclass"/>
		</Item>
		<Item Name="Test Devices" Type="Folder">
			<Item Name="UUTDevice.lvclass" Type="LVClass" URL="../UUTDevice/UUTDevice.lvclass"/>
		</Item>
		<Item Name="Framework" Type="Folder">
			<Item Name="TestSequenceLogWriter.lvclass" Type="LVClass" URL="../../../main/Logging/TestSequenceLogWriter/TestSequenceLogWriter.lvclass"/>
			<Item Name="ExampleTestSequenceRunner.lvclass" Type="LVClass" URL="../ExampleTestSequenceRunner/ExampleTestSequenceRunner.lvclass"/>
		</Item>
		<Item Name="Test API" Type="Folder">
			<Item Name="UUTTestApi.lvclass" Type="LVClass" URL="../UUTTestApi/UUTTestApi.lvclass"/>
		</Item>
		<Item Name="StartTestSystem.vi" Type="VI" URL="../StartTestSystem.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Analog to Digital.vi"/>
				<Item Name="Append Waveforms.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Append Waveforms.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Built App File Layout.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/Built App File Layout.vi"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="Check for Equality.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for Equality.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create ActiveX Event Queue.vi"/>
				<Item Name="Create Directory Recursive.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Create Directory Recursive.vi"/>
				<Item Name="Create Error Clust.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create Error Clust.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="Destroy ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Destroy ActiveX Event Queue.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Digital to Boolean Array.vi"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="DTbl Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Analog to Digital.vi"/>
				<Item Name="DTbl Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Compress Digital.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DTbl Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital to Boolean Array.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DWDT Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Analog to Digital.vi"/>
				<Item Name="DWDT Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital to Boolean Array.vi"/>
				<Item Name="DWDT Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Error Code.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="EventData.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/EventData.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Generate Temporary File Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Generate Temporary File Path.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Handle Open Word or Excel File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Toolkit/Handle Open Word or Excel File.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_Excel.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Excel/NI_Excel.lvclass"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_Gmath.lvlib" Type="Library" URL="/&lt;vilib&gt;/gmath/NI_Gmath.lvlib"/>
				<Item Name="NI_HTML.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/HTML/NI_HTML.lvclass"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_MABase.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MABase.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_PtbyPt.lvlib" Type="Library" URL="/&lt;vilib&gt;/ptbypt/NI_PtbyPt.lvlib"/>
				<Item Name="NI_report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_report.lvclass"/>
				<Item Name="NI_ReportGenerationCore.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_ReportGenerationCore.lvlib"/>
				<Item Name="NI_ReportGenerationToolkit.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/_office/NI_ReportGenerationToolkit.lvlib"/>
				<Item Name="NI_Standard Report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Standard Report/NI_Standard Report.lvclass"/>
				<Item Name="ni_tagger_lv_FlushAllConnections.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_FlushAllConnections.vi"/>
				<Item Name="NI_VariableUtilities.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Variable/NI_VariableUtilities.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="OccFireType.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/OccFireType.ctl"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="Read JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Read JPEG File.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="Version To Dotted String.vi" Type="VI" URL="/&lt;vilib&gt;/_xctls/Version To Dotted String.vi"/>
				<Item Name="Wait On ActiveX Event.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait On ActiveX Event.vi"/>
				<Item Name="Wait types.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait types.ctl"/>
				<Item Name="WDT Append Waveforms CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms CDB.vi"/>
				<Item Name="WDT Append Waveforms CXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms CXT.vi"/>
				<Item Name="WDT Append Waveforms DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms DBL.vi"/>
				<Item Name="WDT Append Waveforms EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms EXT.vi"/>
				<Item Name="WDT Append Waveforms I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I16.vi"/>
				<Item Name="WDT Append Waveforms I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I32.vi"/>
				<Item Name="WDT Append Waveforms I64.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I64.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write BMP Data To Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data To Buffer.vi"/>
				<Item Name="Write BMP Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data.vi"/>
				<Item Name="Write BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP File.vi"/>
				<Item Name="Write JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Write JPEG File.vi"/>
				<Item Name="Write PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Write PNG File.vi"/>
				<Item Name="XControlSupport.lvlib" Type="Library" URL="/&lt;vilib&gt;/_xctls/XControlSupport.lvlib"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="BroadcastEventType.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/BroadcastEventType.ctl"/>
			<Item Name="BroadCastIncomingEvent.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/BroadCastIncomingEvent.ctl"/>
			<Item Name="Calibrate.lvclass" Type="LVClass" URL="../../../main/Calibration/Calibrate/Calibrate.lvclass"/>
			<Item Name="ChannelIdDelimiter.ctl" Type="VI" URL="../../../main/Sequence/SequenceInputRunner/ChannelIdDelimiter.ctl"/>
			<Item Name="Controller.lvclass" Type="LVClass" URL="../../../main/ControlSystem/Controller/Controller.lvclass"/>
			<Item Name="DeviceReader.lvclass" Type="LVClass" URL="../../../main/IOStreams/DeviceReader/DeviceReader.lvclass"/>
			<Item Name="DeviceWriter.lvclass" Type="LVClass" URL="../../../main/IOStreams/DeviceWriter/DeviceWriter.lvclass"/>
			<Item Name="DisplayLogWriter.lvclass" Type="LVClass" URL="../../../main/Logging/DisplayLogWriter/DisplayLogWriter.lvclass"/>
			<Item Name="ExecutionBase.lvclass" Type="LVClass" URL="../../../main/Execution/ExecutionBase/ExecutionBase.lvclass"/>
			<Item Name="FeedbackController.lvclass" Type="LVClass" URL="../../../main/ControlSystem/FeedbackController/FeedbackController.lvclass"/>
			<Item Name="FeedbackControlSequence.lvclass" Type="LVClass" URL="../../../main/Sequence/Concrete/FeedbackControlSequence/FeedbackControlSequence.lvclass"/>
			<Item Name="FileLogWriter.lvclass" Type="LVClass" URL="../../../main/Logging/FileLogWriter/FileLogWriter.lvclass"/>
			<Item Name="FileReader.lvclass" Type="LVClass" URL="../../../main/IOStreams/FileReader/FileReader.lvclass"/>
			<Item Name="FileStream.lvclass" Type="LVClass" URL="../../../main/IOStreams/FileStream/FileStream.lvclass"/>
			<Item Name="FileWriter.lvclass" Type="LVClass" URL="../../../main/IOStreams/FileWriter/FileWriter.lvclass"/>
			<Item Name="FloatingPointSequence.lvclass" Type="LVClass" URL="../../../main/Sequence/FloatingPointSequence/FloatingPointSequence.lvclass"/>
			<Item Name="HtmlFileLogWriter.lvclass" Type="LVClass" URL="../../../main/Logging/HtmlFileLogWriter/HtmlFileLogWriter.lvclass"/>
			<Item Name="IncomingUserEvent.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/IncomingUserEvent.ctl"/>
			<Item Name="InitData.ctl" Type="VI" URL="../../../main/ControlSystem/Regulator/XControl/RegulatorConfiguration/InitData.ctl"/>
			<Item Name="InputStream.lvlib" Type="Library" URL="../../../main/IOStreams/InputStream/InputStream.lvlib"/>
			<Item Name="IO.lvclass" Type="LVClass" URL="../../../main/IOStreams/IO/IO.lvclass"/>
			<Item Name="JumpCluster.ctl" Type="VI" URL="../../../main/Sequence/SequenceInputRunner/JumpCluster.ctl"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="LeadLag.vi" Type="VI" URL="../../../main/ControlSystem/Concrete/PIDRegulator/LeadLag.vi"/>
			<Item Name="ListFilesWithFileTypes.vi" Type="VI" URL="../../../main/Utilities/Path/ListFilesWithFileTypes.vi"/>
			<Item Name="ListLogWriter.lvclass" Type="LVClass" URL="../../../main/Logging/ListLogWriter/ListLogWriter.lvclass"/>
			<Item Name="Logger.lvclass" Type="LVClass" URL="../../../main/Logging/Logger/Logger.lvclass"/>
			<Item Name="LogWriter.lvclass" Type="LVClass" URL="../../../main/Logging/LogWriter/LogWriter.lvclass"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="OutgoingUserEvent.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/OutgoingUserEvent.ctl"/>
			<Item Name="OutgoingUserEventArray.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/OutgoingUserEventArray.ctl"/>
			<Item Name="OutputStream.lvclass" Type="LVClass" URL="../../../main/IOStreams/OutputStream/OutputStream.lvclass"/>
			<Item Name="PIDRegulator.lvclass" Type="LVClass" URL="../../../main/ControlSystem/Concrete/PIDRegulator/PIDRegulator.lvclass"/>
			<Item Name="PidRegulator.xctl" Type="XControl" URL="../../../main/ControlSystem/Concrete/PIDRegulator/XControl/PidRegulator.xctl"/>
			<Item Name="PidRegulatorObjectAttributes.ctl" Type="VI" URL="../../../main/ControlSystem/Concrete/PIDRegulator/PidRegulatorObjectAttributes.ctl"/>
			<Item Name="Regulator.lvclass" Type="LVClass" URL="../../../main/ControlSystem/Regulator/Regulator.lvclass"/>
			<Item Name="Regulator.xctl" Type="XControl" URL="../../../main/ControlSystem/Regulator/XControl/Regulator/Regulator.xctl"/>
			<Item Name="RegulatorObjectAttributes.ctl" Type="VI" URL="../../../main/ControlSystem/Regulator/RegulatorObjectAttributes.ctl"/>
			<Item Name="RingBuffer.lvlib" Type="Library" URL="../../../main/Buffer/RingBuffer/RingBuffer.lvlib"/>
			<Item Name="Sequence.xctl" Type="XControl" URL="../../../main/Sequence/SequenceInputRunner/XControl/Sequence.xctl"/>
			<Item Name="SequenceInputRunner.lvclass" Type="LVClass" URL="../../../main/Sequence/SequenceInputRunner/SequenceInputRunner.lvclass"/>
			<Item Name="SequenceInputRunnerObjectAttributes.ctl" Type="VI" URL="../../../main/Sequence/SequenceInputRunner/SequenceInputRunnerObjectAttributes.ctl"/>
			<Item Name="SequenceSeriesType.ctl" Type="VI" URL="../../../main/Sequence/SequenceInputRunner/SequenceSeriesType.ctl"/>
			<Item Name="SetWaveformChannelNames.vi" Type="VI" URL="../../../main/Utilities/Waveform/SetWaveformChannelNames.vi"/>
			<Item Name="SimulatedBufferedDeviceReader.lvclass" Type="LVClass" URL="../../../drivers/DAQ/SimulatedBufferedDeviceReader/SimulatedBufferedDeviceReader.lvclass"/>
			<Item Name="SimulatedUnbufferedDeviceWriter.lvclass" Type="LVClass" URL="../../../drivers/DAQ/SimulatedUnbufferedDeviceWriter/SimulatedUnbufferedDeviceWriter.lvclass"/>
			<Item Name="Stream.lvclass" Type="LVClass" URL="../../../main/IOStreams/Stream/Stream.lvclass"/>
			<Item Name="SubPanelIncomingData.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/SubPanelIncomingData.ctl"/>
			<Item Name="SubPanelInitiate.vi" Type="VI" URL="../../../main/GUI/SubPanelUtilities/SubPanelInitiate.vi"/>
			<Item Name="SubPanelOutgoingData.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/SubPanelOutgoingData.ctl"/>
			<Item Name="SubPanelOutgoingDataArray.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/SubPanelOutgoingDataArray.ctl"/>
			<Item Name="TestApi.lvclass" Type="LVClass" URL="../../../main/TestSystem/TestApi/TestApi.lvclass"/>
			<Item Name="TestSequence.lvclass" Type="LVClass" URL="../../../main/TestSystem/TestSequence/TestSequence.lvclass"/>
			<Item Name="TestSequenceExecutive.xctl" Type="XControl" URL="../../../main/TestSystem/TestSequence/XControls/Executive/TestSequenceExecutive.xctl"/>
			<Item Name="TestSequenceRunner.lvclass" Type="LVClass" URL="../../../main/TestSystem/TestSequenceRunner/TestSequenceRunner.lvclass"/>
			<Item Name="Threading.lvclass" Type="LVClass" URL="../../../main/Execution/Threading/Threading.lvclass"/>
			<Item Name="UnbufferedDeviceReader.lvclass" Type="LVClass" URL="../../../main/IOStreams/UnbufferedDeviceReader/UnbufferedDeviceReader.lvclass"/>
			<Item Name="UnbufferedDeviceWriter.lvclass" Type="LVClass" URL="../../../main/IOStreams/UnbufferedDeviceWriter/UnbufferedDeviceWriter.lvclass"/>
			<Item Name="url_decode.vi" Type="VI" URL="../../../main/Utilities/String/url_decode.vi"/>
			<Item Name="UserDefinedErrorReasons.lvclass" Type="LVClass" URL="../../../main/Logging/UserDefinedErrorReasons/UserDefinedErrorReasons.lvclass"/>
			<Item Name="UserEventType.ctl" Type="VI" URL="../../../main/GUI/SubPanelUtilities/UserEventType.ctl"/>
			<Item Name="UUTBase.lvclass" Type="LVClass" URL="../../../main/TestSystem/UUTBase/UUTBase.lvclass"/>
			<Item Name="WaitForOpen.vi" Type="VI" URL="../../../main/GUI/SubPanelUtilities/WaitForOpen.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Joysticktestsystem" Type="EXE">
				<Property Name="App_INI_aliasGUID" Type="Str">{C9E66BFB-AB37-41C4-9F2F-21BD5D1FAE10}</Property>
				<Property Name="App_INI_GUID" Type="Str">{BDA151E5-71BD-4581-943B-2600C154C0AC}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{4A8402A8-CF2F-41E0-B4CF-2BFFFB10E82D}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Joysticktestsystem</Property>
				<Property Name="Bld_compilerOptLevel" Type="Int">0</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../joystickcontroller/Builds</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{85BE65CC-EB27-4776-94AA-D6BBA27022AC}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Joysticktest.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../joystickcontroller/Builds/Joysticktest.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../joystickcontroller/Builds/data</Property>
				<Property Name="Destination[2].destName" Type="Str">SupportFiles</Property>
				<Property Name="Destination[2].path" Type="Path">../joystickcontroller/Builds/SupportFiles/HTML</Property>
				<Property Name="Destination[2].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[3].destName" Type="Str">Hexfiles</Property>
				<Property Name="Destination[3].path" Type="Path">/C/Temp/Hexfiles</Property>
				<Property Name="Destination[3].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[3].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="DestinationCount" Type="Int">4</Property>
				<Property Name="Source[0].itemID" Type="Str">{A223EC68-5665-4C69-A761-0A700F5CBEC1}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/StartTestSystem.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[2].itemID" Type="Ref"></Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[3].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">3</Property>
				<Property Name="Source[3].itemID" Type="Ref"></Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">4</Property>
				<Property Name="TgtF_companyName" Type="Str">Prevas AB</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Joysticktestsystem</Property>
				<Property Name="TgtF_internalName" Type="Str">Joysticktestsystem</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2012 Prevas AB</Property>
				<Property Name="TgtF_productName" Type="Str">Joysticktestsystem</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{06C167C5-5067-44AB-A9A2-FACD0885EA4E}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Joysticktest.exe</Property>
			</Item>
			<Item Name="Joystick Testsystem" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">Caldaro Testsystem</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{B8ABCEC6-43F5-445F-BEBD-4E6A06B8286A}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{C34C4274-13B4-47B7-BC33-1D8996632A27}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI LabVIEW Runtime 2014 SP1 f11</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{4722F14B-8434-468D-840D-2B0CD8CBD5EA}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[1].productID" Type="Str">{B1EE55C1-F98B-40AB-AF0C-422ECCC88454}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI Measurement &amp; Automation Explorer 5.0</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{AE940F24-CC0E-4148-9A96-10FB04D9796D}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">_full_</Property>
				<Property Name="DistPart[2].productID" Type="Str">{A108E5C1-C45E-4EA4-A395-CF61F959AC82}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI-DAQmx MAX Configuration Support 9.3.5</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{9856368A-ED47-4944-87BE-8EF3472AE39B}</Property>
				<Property Name="DistPart[3].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[3].productID" Type="Str">{B5C45029-0755-4D0F-BAD2-6CF605D9E8F4}</Property>
				<Property Name="DistPart[3].productName" Type="Str">NI-VISA Runtime 5.1</Property>
				<Property Name="DistPart[3].upgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="DistPart[4].flavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPart[4].productID" Type="Str">{C673968D-BBC5-4A5E-AFF4-60F538388775}</Property>
				<Property Name="DistPart[4].productName" Type="Str">NI-VISA Configuration Support 5.1</Property>
				<Property Name="DistPart[4].upgradeCode" Type="Str">{093EECCF-DE2B-4226-B7E5-B1FD4028A783}</Property>
				<Property Name="DistPartCount" Type="Int">5</Property>
				<Property Name="INST_author" Type="Str">Prevas AB</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">../joystickcontroller/Installer</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">Joystick Testsystem</Property>
				<Property Name="INST_defaultDir" Type="Str">{B8ABCEC6-43F5-445F-BEBD-4E6A06B8286A}</Property>
				<Property Name="INST_includeError" Type="Bool">false</Property>
				<Property Name="INST_productName" Type="Str">Joystick Testsystem</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.6</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">14018027</Property>
				<Property Name="MSI_arpCompany" Type="Str">Prevas AB</Property>
				<Property Name="MSI_arpContact" Type="Str">Roger Isaksson</Property>
				<Property Name="MSI_arpPhone" Type="Str">+46 (0)40-691 95 00</Property>
				<Property Name="MSI_arpURL" Type="Str">http://www.prevas.se</Property>
				<Property Name="MSI_distID" Type="Str">{A5AB402B-7850-40EE-97AC-7BDCBCC0C726}</Property>
				<Property Name="MSI_hwOption" Type="Int">1</Property>
				<Property Name="MSI_hwPath" Type="Path">../joystickcontroller/CONFIGDATA.NCE</Property>
				<Property Name="MSI_hwPath.type" Type="Str">relativeToCommon</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{5F4A99FC-2213-46BC-B008-7A7B33742D33}</Property>
				<Property Name="MSI_windowTitle" Type="Str">Caldaro Testsystem Installer</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{B8ABCEC6-43F5-445F-BEBD-4E6A06B8286A}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{B8ABCEC6-43F5-445F-BEBD-4E6A06B8286A}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">Joysticktest.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">Joysticktest</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">Caldaro Testsystem</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{06C167C5-5067-44AB-A9A2-FACD0885EA4E}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">Joysticktestsystem</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/Joysticktestsystem</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
			<Item Name="Testcase Executive Example" Type="Source Distribution">
				<Property Name="Bld_buildCacheID" Type="Str">{808FAC88-48BF-4EC7-8D8B-B6B5F0C07520}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Testcase Executive Example</Property>
				<Property Name="Bld_excludedDirectory[0]" Type="Path">vi.lib</Property>
				<Property Name="Bld_excludedDirectory[0].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[1]" Type="Path">resource/objmgr</Property>
				<Property Name="Bld_excludedDirectory[1].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[2]" Type="Path">/C/Users/rois/Documents/LabVIEW Data/InstCache</Property>
				<Property Name="Bld_excludedDirectory[3]" Type="Path">instr.lib</Property>
				<Property Name="Bld_excludedDirectory[3].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectory[4]" Type="Path">user.lib</Property>
				<Property Name="Bld_excludedDirectory[4].pathType" Type="Str">relativeToAppDir</Property>
				<Property Name="Bld_excludedDirectoryCount" Type="Int">5</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/Temp/Testcase Executive Example</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{5954AD23-51AC-4603-AE89-0A4E9A4AF955}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">/C/Temp/Testcase Executive Example</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/Temp/Testcase Executive Example/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">SupportFiles</Property>
				<Property Name="Destination[2].path" Type="Path">/C/Temp/Testcase Executive Example/SupportFiles/HTML</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Source[0].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[0].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[0].itemID" Type="Str">{03083572-9CDE-47D3-B695-AA9F89144094}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/StartTestSystem.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Framework/ExampleTestSequenceRunner.lvclass</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Test API/UUTTestApi.lvclass</Property>
				<Property Name="Source[3].type" Type="Str">Library</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Test Devices/UUTDevice.lvclass</Property>
				<Property Name="Source[4].type" Type="Str">Library</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Test Sequences/FlashUUTTestSequence.lvclass</Property>
				<Property Name="Source[5].type" Type="Str">Library</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Test Sequences/ExampleTestSequence.lvclass</Property>
				<Property Name="Source[6].type" Type="Str">Library</Property>
				<Property Name="Source[7].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/SupportFiles</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">8</Property>
			</Item>
		</Item>
	</Item>
</Project>
