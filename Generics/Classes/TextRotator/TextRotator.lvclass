﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="14008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">10444544</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16383904</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,.!!!*Q(C=T:3`DF*"&amp;-9`V%1\QR.I?)54/B/&lt;@16MNTO6U:*33HC%]QI%+EI;49T6K8=L#PN&gt;N$5RQ&gt;]&gt;BL_\#R;;\!S(?_`XH4HT9W;Y5GEPJ'&gt;;H7O8*RM_F1DOC?:_`^LYZ&lt;HWT@XWOP(X&gt;&gt;XDHRN@N?0\S`0]Z^K4E_X9\X:0_&gt;XJ;$LN0O4D.EA('4O`OE=:7X`HLD//`&gt;'"0\PL4X=6:L0:["[`:MRG$`JE4'=H`7I`0H^[Y%_HE]HE=0V(I[X0TZS5&gt;LB`I\7`ZR\P@]GYH/TM/_&gt;H&gt;/$?^7F\\LX_14N\P0GX$=\`#@ZPEVJK8E2%%E%Y97KM6;)H?K)H?K)H?K!(?K!(?K!(OK-\OK-\OK-\OK%&lt;OK%&lt;OK%&lt;?OHI1B?[U&amp;G6:0*EIK2I5C"*"E8*6]+4]#1]#1_03HA3HI1HY5FY3&amp;(#E`!E0!F0QM-Q*4Q*4]+4]#1]F#IE74I[0!E0Z28Q"$Q"4]!4]$#F!JY!)*AM+"Q5!5/"'&gt;Q%0!&amp;0Q-/N!J[!*_!*?!)?&lt;!5]!5`!%`!%0!QJKR+&amp;JOHI]&amp;"'$I`$Y`!Y0!Y0J?8Q/$Q/D]0D]$#&gt;("[(RY&amp;Q*H3+AS"HE*0A0$A]$A]8/4Q/D]0D]$A]7'7(P+R-1^.U&gt;(A-(I0(Y$&amp;Y$"Z+S/!R?!Q?A]@AI;Q-(I0(Y$&amp;Y$"[GEM&amp;D]"A]"IAR+&gt;0,+'9-.*)-Q?$B5U[,F6W+1G+F3`8QKBZ+V=/G?IB5$Y@KJKNOJOIGK3[_[K+K,J&lt;K)KD_/&amp;6I62D6361(.YF;]LUAZM39'"*^IE&gt;=%"WCX1T^RYH,Z6+,R5,T_6TD]6D$Y6$^@F_^8E]8&amp;R@K&gt;$JKN^P;N.@U&gt;7PNPZ?_XLT`?0PO[PLG[NNV=\X^=06DYQX),@(U=WPQ]G&gt;L]0:XCY&amp;`'^](HV;`"K_Y0F^^'&lt;41^+&lt;57\_8(M/\55_UPG\8[!]S_CO9!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.13</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"F]5F.31QU+!!.-6E.$4%*76Q!!&amp;11!!!2Y!!!!)!!!&amp;/1!!!!9!!!!!2.5:8BU5G^U982P=CZM&gt;G.M98.T!!!!E"1!A!!!-!!!+!!!!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!!;53EN0!)`1)%B3Z:$L,KX!!!!$!!!!"!!!!!!FWP"KX'9;U++(U^[;S:!*^1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!)Z&lt;UK:`306(NV-L$0=%,+Y"!!!!`````Q!!!"!&lt;F,R[D_%O3\&gt;*P-^,M;_L!!!!"!!!!!!!!!#/!!&amp;-6E.$!!!!!A!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF$1Q!!!!!"&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!&amp;"53$!!!!!&lt;!!%!!Q!!&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!!!!!A!"`Q!!!!%!!1!!!!!!$1!!!!!!!!!!!!!!!!!!!!!!!Q!!!!!!!A!#!!!!!!!D!!!!'HC=9W"H9'ZAO-!!R)Q/4!V-'5$7"Q9'$A9'!%H6"6!!!!!!2A!!!2BYH'.AQ!4`A1")-4)Q-&amp;U!UCRIYG!;RK9GQ'5O,LOAYMR1.\,#B)(OXA/EG5"S5$610T#&gt;!?)4[/;Q9T%&lt;!(`T+#5!!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!'0!!!#D(C=D6+`3].!&amp;([P"LV+)!'$:"#MG+')#')2BQ\6JN#B90W"1Q?8KF11\.#BAU/E&gt;AC(A^$"R=X.45(K)CUJJ+0A8_$7I9/L5XVX3;3$AA]OXXW0&gt;^`X=2=(!5\8.N9`#3&gt;I'?"!_@TI?"K)1V#V'0R5.M46DC&gt;R$Q0?'+L=NP4'B78'N+:"(6[Q$,&gt;A+;ZC$?2%4`6Y`MT@4!P''@6^WW+#%#I#2TM6-@6%WS+J-;?_#.K6)OV+HO_E1L.)MO4"/U4_/JV)O+2%/"?)K['Y."H%:1D&gt;ARE5][;=MSW$AC9IK%Z\F:"RY0(RY-W/VOJIT`WW=.&gt;SX5&lt;0^/2&gt;"&lt;[-TOP&gt;$Y6@PF(P_E:]K;7[_]TU&lt;&lt;9]3V09&amp;]&amp;=.&lt;I*ZJ8$]Q&gt;#I%&lt;G+2&amp;9%6UZQS@^4,)PYG?-7QG6$!KIZS7L/M4;YE.KBRZM@9VQ/&amp;)7CO'\,=(P&gt;5)L$1APB#O%][3!9%)6AX[,-%EIAER"]0!)$!KYCX&gt;YDQ`YC#`YCH`)`[OSWX:OH%@`VT=O#H?;!!!!!"-!!!!*?*RD9'"A:'1!!A!!&amp;!!$!!!!!!Y5!9!H!!!'-41O-#YR!!!!!!!!$"1!A!!!!!1R.#YQ!!!!!!Y5!9!H!!!'-41O-#YR!!!!!!!!$"1!A!!!!!1R.#YQ!!!!!!Y5!9!H!!!'-41O-#YR!!!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!```````````%\'RP\6&lt;7V_T7RN@N6N&lt;8\7\7\```````````A!!!!9!!!!'!!]!"A!`Q!9!``!'!``]"A0``!9$``Q'!``]"A0``!9$``Q'!``]"A0``!9$``Q'!``]"A0``Y9"```G!(``BA!@`Q9!"`Q'!!$Q"A!!!!@````]!!!)!``````````````````````O\O\O\O\O\O\O\O\O\O\`\!!M!O\#\!!OQOQ!,M,O`_\#\#QM,#\#\#QOQOQM,P`OQOQ#\#QOQOQ!,M,M,#\`\M,M,#QM,M,M,#\#\#QO`_\#\#QOQO\#\#QOQO\#\P`O\O\O\O\O\O\O\O\O\O\``````````````````````]2%2%2%2%2%2%2%2%2%2(`%2%2%2%2%&lt;M2%2%2%2%2`R%2%2%2%&lt;_P_R%2%2%2%@]2%2%2%&lt;_LO\`\%2%2%2(`%2%2%&lt;_LO\O\P`M2%2%2`R%2%2_LO\O\O\O`]2%2%@]2%2%;K\O\O\O\P`%2%2(`%2%2'P_LO\O\P`_B%2%2`R%2%2L``[O\P```I2%2%@]2%2%;````L````[%2%2(`%2%2'P````````_B%2%2`R%2%2L`````````I2%2%@]2%2%;`````````[%2%2(`%2%2'P````````_B%2%2`R%2%2L`````````I2%2%@]2%2%@````````````%2(`%2%2%;L```````L```]2`R%2%2%2L`````L```]2%@]2%2%2%2'P``P````R%2(`%2%2%2%2%;P````R%2%2`R%2%2%2%2%2(``R%2%2%@]2%2%2%2%2%2%2%2%2%2(`````````````````````]!!!1!````````````````````````````````````````````8V^@8V^@8V^@8V^@8V^@8V^@8V^@8V^@8V^@8V^@``^@!!!!8Q!!8V^@!&amp;^@!!!!8V]!8V]!!!"@8Q"@8V```V^@!&amp;^@!&amp;]!8Q"@!&amp;^@!&amp;^@!&amp;]!8V]!8V]!8Q"@8```8V]!8V]!!&amp;^@!&amp;]!8V]!8V]!!!"@8Q"@8Q"@!&amp;^@``^@8Q"@8Q"@!&amp;]!8Q"@8Q"@8Q"@!&amp;^@!&amp;^@!&amp;]!8V```V^@!&amp;^@!&amp;]!8V]!8V^@!&amp;^@!&amp;]!8V]!8V^@!&amp;^@8```8V^@8V^@8V^@8V^@8V^@8V^@8V^@8V^@8V^@8V^@`````````````````````````````````````````````Q)#!A)#!A)#!A)#!A)#!A)#!A)#!A)#!A)#!A)#!P``!A)#!A)#!A)#!A)#!A)V.1)#!A)#!A)#!A)#!A)#``]#!A)#!A)#!A)#!A)VL9GNL45#!A)#!A)#!A)#!A,``Q)#!A)#!A)#!A)VL9F@8V^@L;UV!A)#!A)#!A)#!P``!A)#!A)#!A)VL9F@8V^@8V^@8[WN.1)#!A)#!A)#``]#!A)#!A)#L9F@8V^@8V^@8V^@8V_NL1)#!A)#!A,``Q)#!A)#!A+*C6^@8V^@8V^@8V^@8\/N!A)#!A)#!P``!A)#!A)#!IGNL9F@8V^@8V^@8\/TMYE#!A)#!A)#``]#!A)#!A)#C;WNL;W*8V^@8\/TM\/TC1)#!A)#!A,``Q)#!A)#!A+*L;WNL;WNC;WTM\/TM\/*!A)#!A)#!P``!A)#!A)#!IGNL;WNL;WNM\/TM\/TMYE#!A)#!A)#``]#!A)#!A)#C;WNL;WNL;WTM\/TM\/TC1)#!A)#!A,``Q)#!A)#!A+*L;WNL;WNL&lt;/TM\/TM\/*!A)#!A)#!P``!A)#!A)#!IGNL;WNL;WNM\/TM\/TMYE#!A)#!A)#``]#!A)#!A)#C;WNL;WNL;WTM\/TM\/TC1)#!A)#!A,``Q)#!A)#!A+NL;WNL;WNL&lt;/TM\/TM[WNL+SM!A)#!P``!A)#!A)#!A+*C;WNL;WNM\/TM[WNC;SML+SML!)#``]#!A)#!A)#!A)#C;WNL;WTM[WNC;SML+SML!)#!A,``Q)#!A)#!A)#!A)#!IGNL;WN8[SML+SML+Q#!A)#!P``!A)#!A)#!A)#!A)#!A+*8[SML+SML+Q#!A)#!A)#``]#!A)#!A)#!A)#!A)#!A)#!KSML+Q#!A)#!A)#!A,``Q)#!A)#!A)#!A)#!A)#!A)#!A)#!A)#!A)#!A)#!P```````````````````````````````````````````Q!!!!)!!A!!!!!!BA!"2F")5!!!!!%!!F2%1U-!!!!"&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!&amp;"53$!!!!!&lt;!!%!!Q!!&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!!!!!A!!`Q!!!!%!!1!!!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!!"!!!!#V"53$!!!!!!!!!!!!!$!!!!!!15!!!+2XC=L6:&lt;;"28'0\0:#)HGQ&lt;0*.&amp;E)7(8:49'MQ&amp;2KT%E'MSRL4('3QR95?LC2CV)NS3&lt;6#CI$W-A"?F$)+"N7FN+?HHR)9C_##,LD8X1"R%PF55@^%HMC[3ETI\`G&gt;H:O7SS[5.W94A-Z`O```T@^ZV&gt;A%!XKZ&gt;S=&amp;I(QN\BIF?(1#*$!.+N&amp;0+@JD&amp;A!_1`),6"IE-8(7$0J"RJU/'D2+;*LO8D]!`O.HYR9P#1H'!P=/MS&amp;M2C!2W7*T*V3I_;:?LF"H7]X+[K1#-\4X,3!45U3]^L331%L6E]F6;3!]*8S&lt;)7W20`;F"4R&gt;O+6BIU3V&lt;IQ(CG;ED.LM;+3(X4,#GVE)D59:=%,.E--T-T$ECR1%VG'WW))2%!MFZK+9'JZJH'945&lt;-T%"%Y-][WU?(MJ.C.Y&amp;S!_NY2G'5-3&gt;SB^:9'W=XD5P\I1/N7JW'QX3&amp;YGHZ66\N;N!A+3`I-9V9Z4MFFK50K'#O&lt;=;B9CW%^;0[XY&gt;VGA:[1D)^HC0Q)1JAWT,U#&amp;E_-35I9+0-X"UG&amp;B%"S[H)YX&gt;*U?'5Y.$Y?3R].'4]?(B].&gt;$8Y\'5Y0B2$Q6,V;IEW?#G]4J":FJ$FA"-BS#T^T44M,U^$1/!*]/&gt;!N#[^2M!;?9J_EN4$TB4%[Q/J0&lt;CJ0D`_Y`,K98&lt;:?]HKVU0,M/0;MQ@VM?TX[]^*\&gt;C&amp;9[\@-M=0A5"ELY&lt;Z-&amp;=DS,_Q%/!#_";50-'&lt;&gt;H%=-2-\#Y:T=8?6:A@:[&gt;H*TU?\;^Y&amp;G:%-OTWA0DP@&amp;?/0??]:SUQFW8=R-I"T+&amp;R:8R&amp;DEW)I=9@ZE^`A&lt;&lt;K`RSK-&gt;LVK\3!I1FM!R&lt;O8`Q6'J@-B60*9&gt;]9G$$D;:6KH&lt;GXW"]EC)_4/&lt;:5)]AK]E@X2/B5&gt;&amp;7HS&gt;#RBM!!W@XP\J3)V$MCW_+ES,D&gt;[VTUA74]JUP+?+TQ&gt;&amp;LY;3AM\&gt;&amp;0Y@I18$S5O&lt;.SW^/8CZY]G)XZ]H,$UO@FR`HSUM(R'"8#?^0&amp;?5FBKB/+07\]&amp;.28G,)UWHTF-L,T]6ZC5((YHGZ6*B`=7J?'Y^&amp;;G&lt;R9NI-36&gt;K^K)IS'?[,A&gt;B7!/(P#*-,XD$"R?RZT,4HFJ%:-;H2+5/P[0.-/(,L&lt;*5F+V(%[A1Q.-^?@IXHGZE\*)3MK*CH8XWLM$HVX&gt;=[^OO^3U;L!AZ0(^90+-7TQX\=GYW?:"B38BQ4H]CTTIV7_F+8*X*UW9L:SCG=H.T=]C*4WN/WCIYCXZ6R2OX5``SR&lt;$';Y/&amp;9XD9(=.&lt;U7]F+YFSUVA:&gt;NTH]CR^&gt;HO%CAN6@+V&gt;Z:D8=\8X8V\Z^&gt;8X]_WW`\L2@J5&lt;J4;Q\&gt;D.'2V7]/\K'WS(]"P&gt;1&lt;?T+5Q`8A1L[22^FXZ5_(/8\CUAN1&gt;E2LKO8AT2_A]P,@"#!!!!"!!!!$Y!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!$'Q!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S&amp;!#!!!!!!!%!#!!Q`````Q!"!!!!!!"\!!!!!Q!/1$$`````"&amp;2F?(1!!%E!]=`CN;)!!!!#%V2F?(23&lt;X2B&gt;'^S,GRW9WRB=X-54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!'%"1!!%!!!N5:8BU5G^U982P=A!=1&amp;!!!1!"%V2F?(23&lt;X2B&gt;'^S,GRW9WRB=X-!!1!#!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=B1!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!A!!!!!!!!!"!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!5!)!!!!!!!1!&amp;!!=!!!%!!-`CN;1!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ&amp;!#!!!!!!!%!"1!(!!!"!!$0YL7E!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9R1!A!!!!!!"!!A!-0````]!!1!!!!!!?Q!!!!-!$E!Q`````Q25:8BU!!"*!0(0YL7C!!!!!B.5:8BU5G^U982P=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"B!5!!"!!!,6'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)!(%"1!!%!!2.5:8BU5G^U982P=CZM&gt;G.M98.T!!%!!A!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF&amp;!#!!!!!!!%!"1!$!!!"!!!!!!!%!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U921!A!!!!!!$!!Z!-0````]%6'6Y&gt;!!!31$RT_+VIA!!!!)46'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!91&amp;!!!1!!#V2F?(23&lt;X2B&gt;'^S!"R!5!!"!!%46'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=Q!"!!)!!!!!!!!!!!!!!!!%!!5!#A!!!!1!!!#K!!!!+!!!!!)!!!1!!!!!&amp;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%N!!!#28C=H6""3M.!&amp;(X*R$;VM;UWWCI)MX,BIACOB9AAO,)5=7W;4+13C#1`J=O?2&lt;S#Z`!/HE"0I(]G!1MK"@0)H`FP:B\P@1!([!1HH`QZ.WJ"Q"5_8N^?HA$9@=V--AIJSU@J0%L$IP#PJQ]KIH/C@$9N327DC&amp;)-AT%MI,XS!,&gt;'[0EPI?YKJT7/D9:V?*'7";F=:IEU&amp;_6D0JO(J'1=5IA."D92!^9\"$?C&lt;H(+43NI1C4J0&gt;RA+&lt;)*694+%P298;!*&amp;SV2RAGP*D4/`B08A\&lt;;BG=M?Y)7-:M9Q_;["@_892Q:LLMWH!UO$6V9Q=5&gt;_X*QS&gt;)3W^DB""J^-Y5+IM:0ZPP%.[CG:''X.L.OUA[U!R-&amp;(;YW-QX?^&lt;#(!9&lt;]$\#P&amp;&lt;]!`8O#+Q!!!!!!!'5!!1!#!!-!"!!!!%A!$Q1!!!!!$Q$9!.5!!!"2!!]%!!!!!!]!W!$6!!!!7A!0"!!!!!!0!.A!V1!!!'/!!)1!A!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-!!!!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!"5%!!!%?!!!!#!!!"4E!!!!!!!!!!!!!!!A!!!!.!!!"'A!!!!?4%F#4A!!!!!!!!&amp;Y4&amp;:45A!!!!!!!!'-5F242Q!!!!!!!!'A1U.46!!!!!!!!!'U4%FW;1!!!!!!!!()1U^/5!!!!!!!!!(=6%UY-!!!!!!!!!(Q2%:%5Q!!!!!!!!)%4%FE=Q!!!!!!!!)96EF$2!!!!!!!!!)M2U.%31!!!!!!!!*!&gt;G6S=Q!!!!1!!!*55U.45A!!!!!!!!+Y2U.15A!!!!!!!!,-35.04A!!!!!!!!,A;7.M.!!!!!!!!!,U;7.M/!!!!!!!!!-)1V"$-A!!!!!!!!-=4%FG=!!!!!!!!!-Q2F")9A!!!!!!!!.%2F"421!!!!!!!!.96F"%5!!!!!!!!!.M4%FC:!!!!!!!!!/!1E2)9A!!!!!!!!/51E2421!!!!!!!!/I6EF55Q!!!!!!!!/]2&amp;2)5!!!!!!!!!01466*2!!!!!!!!!0E3%F46!!!!!!!!!0Y6E.55!!!!!!!!!1-2F2"1A!!!!!!!!1A!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!=!!!!!!!!!!$`````!!!!!!!!!,!!!!!!!!!!!0````]!!!!!!!!!R!!!!!!!!!!!`````Q!!!!!!!!$-!!!!!!!!!!$`````!!!!!!!!!7!!!!!!!!!!!0````]!!!!!!!!";!!!!!!!!!!!`````Q!!!!!!!!'1!!!!!!!!!!$`````!!!!!!!!!&gt;Q!!!!!!!!!!0````]!!!!!!!!"\!!!!!!!!!!!`````Q!!!!!!!!/!!!!!!!!!!!4`````!!!!!!!!!ZA!!!!!!!!!"`````]!!!!!!!!$L!!!!!!!!!!)`````Q!!!!!!!!/]!!!!!!!!!!H`````!!!!!!!!!^!!!!!!!!!!#P````]!!!!!!!!$Y!!!!!!!!!!!`````Q!!!!!!!!0U!!!!!!!!!!$`````!!!!!!!!"!Q!!!!!!!!!!0````]!!!!!!!!%)!!!!!!!!!!!`````Q!!!!!!!!3E!!!!!!!!!!$`````!!!!!!!!"KA!!!!!!!!!!0````]!!!!!!!!+L!!!!!!!!!!!`````Q!!!!!!!!KU!!!!!!!!!!$`````!!!!!!!!#U!!!!!!!!!!!0````]!!!!!!!!07!!!!!!!!!!!`````Q!!!!!!!!^A!!!!!!!!!!$`````!!!!!!!!$WA!!!!!!!!!!0````]!!!!!!!!0?!!!!!!!!!!!`````Q!!!!!!!!`A!!!!!!!!!!$`````!!!!!!!!$_A!!!!!!!!!!0````]!!!!!!!!4#!!!!!!!!!!!`````Q!!!!!!!"-1!!!!!!!!!!$`````!!!!!!!!%RA!!!!!!!!!!0````]!!!!!!!!42!!!!!!!!!#!`````Q!!!!!!!"2Y!!!!!!^5:8BU5G^U982P=CZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!2.5:8BU5G^U982P=CZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!!!!!1!"!!!!!!!.!!!!!!-!$E!Q`````Q25:8BU!!"*!0(0YL7C!!!!!B.5:8BU5G^U982P=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"B!5!!"!!!,6'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)!6A$RT_+VJ!!!!!)46'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=Q^5:8BU5G^U982P=CZD&gt;'Q!+E"1!!%!!2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!)!!!!#!!!!!!!!!!%!!!!!!!!!!!!"%62I=G6B:'FO:SZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!"%!A!A!!!!!!!!!!!!!</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"3!!!!!2&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=V"53$!!!!!U!!%!#1!!!!!!"'VB;7Y*28BF9X6U;7^O#62I=G6B:'FO:R&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="TextRotator.ctl" Type="Class Private Data" URL="TextRotator.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074266624</Property>
		</Item>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="InitLoop.vi" Type="VI" URL="../InitLoop.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%:!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!6%V2F?(23&lt;X2B&gt;'^S,GRW9WRB=X-!$V2F?(23&lt;X2B&gt;'^S)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#Z!=!!?!!!6%V2F?(23&lt;X2B&gt;'^S,GRW9WRB=X-!$F2F?(23&lt;X2B&gt;'^S)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">50331776</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="DynamicMethods" Type="Folder">
		<Item Name="DynamicMethod.vi" Type="VI" URL="../DynamicMethod.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B4&gt;'^Q4'^P=!!!,E"Q!"Y!!"546'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=Q!06'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)A&lt;X6U!!J!)12*&lt;GFU!!!71&amp;!!!Q!!!!%!!ABF=H*P=C"J&lt;A!!(%"Q!!A!!!!#!!!0476U;'^E5G6G:8*F&lt;G.F!"R!=!!)!!!!!A!!$V2I=G6B:&amp;*F:G6S:7ZD:1!O1(!!(A!!&amp;2.5:8BU5G^U982P=CZM&gt;G.M98.T!!Z5:8BU5G^U982P=C"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!(!!A!#1!+!!M$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!#!!!!!I!!!!)!!!!#!!!!*)!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">32896</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714384</Property>
		</Item>
		<Item Name="MethodPrototype.vi" Type="VI" URL="../MethodPrototype.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B4&gt;'^Q4'^P=!!!,E"Q!"Y!!"546'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=Q!06'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)A4X6U!":!5!!$!!!!!1!##'6S=G^S)'FO!!!=1(!!#!!!!!)!!!^5;(*F9723:7:F=G6O9W5!,E"Q!"Y!!"546'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=Q!/6'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!"!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="Reverse.vi" Type="VI" URL="../Reverse.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%C!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B4&gt;'^Q4'^P=!!!,E"Q!"Y!!"546'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=Q!/6'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)A;7Y!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!(%"Q!!A!!!!#!!!06'BS:7&amp;E5G6G:8*F&lt;G.F!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!'!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="Rotate.vi" Type="VI" URL="../Rotate.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%C!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B4&gt;'^Q4'^P=!!!,E"Q!"Y!!"546'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=Q!/6'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)A;7Y!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!(%"Q!!A!!!!#!!!06'BS:7&amp;E5G6G:8*F&lt;G.F!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!'!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="TextRotatorMain.vi" Type="VI" URL="../TextRotatorMain.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!?!!!!!A!%!!!!%A$Q!!%!!!-!!!!!!!!!!!!!!1!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
		</Item>
		<Item Name="TextRotator_Create.vi" Type="VI" URL="../TextRotator_Create.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!6%V2F?(23&lt;X2B&gt;'^S,GRW9WRB=X-!$V2F?(23&lt;X2B&gt;'^S)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#B!=!!?!!!8&amp;56Y:7.V&gt;'FP&lt;E*B=W5O&lt;(:D&lt;'&amp;T=Q!'5'&amp;S:7ZU!!!O1(!!(A!!&amp;2.5:8BU5G^U982P=CZM&gt;G.M98.T!!Z5:8BU5G^U982P=C"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#A!!!!I!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107825168</Property>
		</Item>
		<Item Name="GetText.vi" Type="VI" URL="../GetText.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%H!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!-0````]%6'6Y&gt;!!!,E"Q!"Y!!"546'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=Q!06'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,E"Q!"Y!!"546'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=Q!/6'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="StartThreads.vi" Type="VI" URL="../StartThreads.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%0!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!6%V2F?(23&lt;X2B&gt;'^S,GRW9WRB=X-!$V2F?(23&lt;X2B&gt;'^S)'^V&gt;!!71&amp;!!!Q!!!!%!!ABF=H*P=C"J&lt;A!!,E"Q!"Y!!"546'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)O&lt;(:D&lt;'&amp;T=Q!/6'6Y&gt;&amp;*P&gt;'&amp;U&lt;X)A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="StopThreads.vi" Type="VI" URL="../StopThreads.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%:!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!6%V2F?(23&lt;X2B&gt;'^S,GRW9WRB=X-!$V2F?(23&lt;X2B&gt;'^S)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#Z!=!!?!!!6%V2F?(23&lt;X2B&gt;'^S,GRW9WRB=X-!$F2F?(23&lt;X2B&gt;'^S)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
		</Item>
		<Item Name="Destroy.vi" Type="VI" URL="../Destroy.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%:!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!6%V2F?(23&lt;X2B&gt;'^S,GRW9WRB=X-!$V2F?(23&lt;X2B&gt;'^S)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#Z!=!!?!!!6%V2F?(23&lt;X2B&gt;'^S,GRW9WRB=X-!$F2F?(23&lt;X2B&gt;'^S)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!)E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117786640</Property>
		</Item>
	</Item>
</LVClass>
